//Estos son los primeros 4 ejercicios de mircha

//Este es una funcion que cuenta la cantidad de caracteres en una cadena string.
//me falta algo que corregir en la validador de array-
let cadena = "Hola mundo soy Fernando";

let arreglo = new Array(10);

const objeto = {
    name: 'Fernando',
    age: 23
}


function contarCadena(palabra = "") {
    if (!palabra) {
        console.warn('No ingresaste ninguna cadena');
    } else {
        console.info(`la cadena tiene ${palabra.length}`);
    }
}

contarCadena(cadena);
contarCadena(arreglo);
contarCadena();

const contarCaracteres = (palabra = "") => (!palabra) ? console.warn('No has ingresado ese valor') : console.info(`El valor es ${palabra.length} en ternario`);
contarCaracteres('Hola soy fernando');


//------------------------------------------------------------------------------------------------------//
//es la segunda tarea de mircha
//Esta es una funcion que recorta los caracteres de una cadena de texto. ya esta realizada.

const recortar = (palabra = "", corte = undefined) => {
    if (!palabra || !corte) {
        return console.warn('No has ingresado palabra o corte');
    } else {
        let frase = palabra.slice(0, corte);
        return frase;

    }
}

let palabraCortada = recortar("Hola mundo", 4);
console.log(palabraCortada);

//Esta es la tercer tarea, funcion que se le entregue un string y te retorna y array de textos separados por un caracter

const arregloString = (cadenaTexto = "") => cadenaTexto.split(" ");

let arregloCadena = arregloString('hola que tal');
// console.log(arregloCadena);

const separadorString = (cadenaTexto = "", separador = undefined) =>
    (!cadenaTexto)
        ? console.warn('No has ingresado la cadena de texto')
        : (separador === undefined)
            ? console.warn('No has ingresado el separador')
            : (console.info(cadenaTexto.split(separador)));



separadorString("", "-");


//Esta es la cuarta tarea, ser basa en una funcion que repita un texto tantas veces el usuario desea.


const repetirTexto = (texto = "", veces = undefined) => {
    return texto.repeat(veces);

}

const multiplicarTexto = (texto = "", veces = undefined) =>
    (!texto)
        ? console.warn('No has ingresado el texto')
        : (veces === undefined)
            ? console.warn('No has ingresado el numero de veces')
            : (console.info(texto.repeat(veces)));

// resultadoTexto = repetirTexto('Hola Mundo ', 3);
// console.log(resultadoTexto);

multiplicarTexto('Hola mundo ', 10);