//5) Programa una función que invierta las palabras de una cadena de texto, pe. miFuncion("Hola Mundo") devolverá "odnuM aloH".
//6) Programa una función para contar el número de veces que se repite una palabra en un texto largo, pe. miFuncion("hola mundo adios mundo", "mundo") devolverá 2.
//7) Programa una función que valide si una palabra o frase dada, es un palíndromo (que se lee igual en un sentido que en otro), pe. mifuncion("Salas") devolverá true.
//8) Programa una función que elimine cierto patrón de caracteres de un texto dado, pe. miFuncion("xyz1, xyz2, xyz3, xyz4 y xyz5", "xyz") devolverá  "1, 2, 3, 4 y 5.


const revertirFrase = (frase = "") => {
    if (!frase) return console.warn('Ingrese una frase por favor');
    if (frase.length === 0) return console.warn('Favor ingrese una frase mas larga');
    return console.log(frase.split('').reverse().join(' '));
}
revertirFrase('Hola mundo');

const contarPalabra = (frase = '', palabra = undefined) => {
    if (!frase) return console.warn('Ingrese una frase por favor');
    if (!palabra) return console.warn('Ingrese una palabra por favor');
    if (frase.length === 0) return console.warn('Favor ingrese una frase mas larga');
    if (palabra.length === 0) return console.warn('Favor ingrese una frase mas larga');

    let contador = 0;
    let i = 0;


    while (i !== -1) {
        i = frase.indexOf(palabra, i);
        if (i !== -1) {
            i++;
            contador++;
        }
    }

    return console.info(`La palabra se repite tantas veces ${contador}`);
}

contarPalabra("hola mundo adios mundo", "hola");
console.clear();



const frasePalindromo = (frase = '') => {
    if (!frase) return console.warn('Ingrese una frase por favor');
    frase = frase.toLowerCase();
    let fraseAlreves = frase.split('').reverse().join('');

    return console.log(frase === fraseAlreves ? 'Es palindromo' : 'No es palindromo');

}

frasePalindromo('salas');

console.clear();


const eliminarPatron = (frase = "", remplazo = "") => {
    if (!frase) return console.warn('Ingrese una frase por favor');
    if (!remplazo) return console.warn('Ingrese una frase por favor');

    let nuevaCadena = frase.replace(new RegExp(remplazo, "ig"), "");
    return console.log(nuevaCadena);
}


eliminarPatron("xyz1,xyz2,xyz3,xyz4,xyz5", "xyz");

console.clear();

//9) Programa una función que obtenga un numero aleatorio entre 501 y 600.
//10) Programa una función que reciba un número y evalúe si es capicúa o no (que se lee igual en un sentido que en otro), pe. miFuncion(2002) devolverá true.
//11) Programa una función que calcule el factorial de un número (El factorial de un entero positivo n, se define como el producto de todos los números enteros positivos desde 1 hasta n), pe. miFuncion(5) devolverá 120.

const numeroAleatorio = (min = 500, max = 600) => {
    return console.log(Math.round(Math.random() * (max - min) + min))
}

numeroAleatorio();


const numCapicua = (numero) => {
    numero = numero.toString();
    let numCovertido = numero.split('').reverse().join('');
    return console.log(numCovertido === numero ? 'Es capicua' : 'no es capicua');
}


numCapicua(333);

console.clear();

const calcularFactorial = (numero = undefined) => {
    if (!numero) return console.warn('Ingrese una frase por favor');
    let factorial = 1;
    for (let i = numero; i > 1; i--) {
        factorial = factorial * i;
    }

    return console.log(factorial);
}


calcularFactorial(5);

console.clear();


//12) Programa una función que determine si un número es primo (aquel que solo es divisible por sí mismo y 1) o no, pe. miFuncion(7) devolverá true.
//13) Programa una función que determine si un número es par o impar, pe. miFuncion(29) devolverá Impar.
//14) Programa una función para convertir grados Celsius a Fahrenheit y viceversa, pe. miFuncion(0,"C") devolverá 32°F.

const numerosPrimos = (num = undefined) => {
    if (!num || num === undefined) return console.warn('Por favor ingrese un numero valido');
    if (Math.sign(num) === -1) return console.warn('Numeros negativos no son admitidos');
    if (typeof (num) !== 'number') return console.warn('No se acepta otro formato que no sea numero');
    (num % 2 === 0) ? console.log('No es primo') : console.log('Es primo');
}

numerosPrimos(6);
console.clear();



const convertirTemperatura = (grados, escala) => {

    if (escala.length !== 1 || !/(c|f)/.test(escala)) return console.log('Valor no valido');//Esta validacion es bien curiosa, debo de poner atencion...
    switch (escala) {
        case 'c':
            let resultado = (grados * 9 / 5 + 32);
            console.log(`El resultado de grados celcius a farenheit es de ${Math.round(resultado)} grados`);
            break;
        case 'f':
            let resultado2 = (grados - 32) * 5 / 9;
            console.log(`El resultado de grados farenheit a celcius es de ${Math.round(resultado2)} grados`);
            break;
    }

}

convertirTemperatura(90, 'f');



const numeroDivisible = (numero) => {
    let divisible = false;
    for (let i = 2; i < numero; i++) {
        if ((numero % 2) === 0) {
            divisible = true;
            break;
        }
    }
    return (divisible) ? console.log('Es divisible') : console.log('No es divisible');
}


numeroDivisible(9);

console.clear();



//15) Programa una función para convertir números de base binaria a decimal y viceversa, pe. miFuncion(100,2) devolverá 4 base 10.
//16) Programa una función que devuelva el monto final después de aplicar un descuento a una cantidad dada, pe. miFuncion(1000, 20) devolverá 800.
//17) Programa una función que dada una fecha válida determine cuantos años han pasado hasta el día de hoy, pe. miFuncion(new Date(1984,4,23)) devolverá 35 años (en 2020).





const numBinariosConverter = (num1, num2) => {
    return console.log(parseInt(num1, num2));
}

numBinariosConverter(100, 2);

const descuento = (cantidad = undefined, descuento = 0) => {

    let descuentoFinal = (descuento / 100) * cantidad;
    return console.log(`El precio del producto es ${cantidad}$ y tiene un descuento del ${descuento}% en la rebaja seria de ${descuentoFinal}$ y quedaria en ${cantidad - descuentoFinal}$ muchas gracias!`)
}


descuento(1000, 20);

console.clear();

const fecha = (fecha = undefined) => {
    if (fecha === undefined || fecha.length === 0) return console.log('has instroducido el valor incorrecto');
    if (!(fecha instanceof Date)) return console.warn('Has introducido un formato que no es valido');
    let hoy = new Date();
    return console.log(parseInt((hoy - fecha) / (1000 * 60 * 60 * 24 * 365)));
}

fecha(new Date(new Date(1984, 4, 23)));
console.clear();


//18) Programa una función que dada una cadena de texto cuente el número de vocales y consonantes, pe. miFuncion("Hola Mundo") devuelva Vocales: 4, Consonantes: 5.
//19) Programa una función que valide que un texto sea un nombre válido, pe. miFuncion("Jonathan MirCha") devolverá verdadero.
//20) Programa una función que valide que un texto sea un email válido, pe. miFuncion("jonmircha@gmail.com") devolverá verdadero

const contarLetras = (frase = "") => {
    if(!frase || frase.length === 0) return console.log('Por favor ingrese una frase')
    if(typeof(frase)!=='string') return console.log('Formato no valido');
    let vocales = frase.match(/[aeiou]/gi).length;
    let consonantes = frase.match(/[bcdfghjklmnñpqrstvwxyz]/gi).length;
    return console.log(`Estas son las vocales ${vocales} y estas son las consonantes ${consonantes}`);
}
contarLetras('Hola Mundo');
console.clear();


const validadorTexto = (texto="") =>{
    let expReg = /^[A-Za-z\s]+$/g.test(texto);
    return(expReg) ? console.log('Es un nombre valido'): console.log('No es un nombre valido');
    
}


console.clear();

const validarEmail = (mail="") =>{
    if(!mail) return console.warn('Por favor ingrese un mail');
    let emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/i;
    mail.match(emailRegex)? console.log('Si es valido'): console.log('No es valido');
}


validarEmail();

console.clear();

//21) Programa una función que dado un array numérico devuelve otro array con los números elevados al cuadrado, pe. mi_funcion([1, 4, 5]) devolverá [1, 16, 25].
//22) Programa una función que dado un array devuelva el número mas alto y el más bajo de dicho array, pe. miFuncion([1, 4, 5, 99, -60]) devolverá [99, -60].
//23) Programa una función que dado un array de números devuelva un objeto con 2 arreglos en el primero almacena los números pares y en el segundo los impares, pe. miFuncion([1,2,3,4,5,6,7,8,9,0]) devolverá {pares: [2,4,6,8,0], impares: [1,3,5,7,9]}.


let arreglo = [1, 4, 5,6,7,8,9];

const cuadrado = (arreglo) => {
    let potencia = arreglo.map((elemento)=>{
        return console.log((elemento**2))
    });
    
    return potencia;
}
cuadrado(arreglo);


const maxMin = (arreglo) =>{
    return console.log({
        numMax: `${Math.max(...arreglo)}`,
        numMin: `${Math.min(...arreglo)}`
    });
}


maxMin(arreglo);

console.clear();

let paresImpares = (arreglo=undefined) =>{
    if(arreglo===undefined) return console.warn('No ingresaste un arreglo de numeros');
    if(!(arreglo instanceof Array)) return console.warn('Esto no es una instancia de arreglo');
    if(arreglo.length=== 0) return console.log('El arreglo no debe estar vacio');
    for (let num of arreglo){
        if(typeof num !== 'number'){
            return console.log('Solo acepta valores numericos');
        }
    }
    return console.log(
        {
            pares: arreglo.filter(num => num % 2 === 0),
            impares: arreglo.filter(num => num % 2 ===1)
        }
    );
}

paresImpares(34343434);
paresImpares([]);
paresImpares([3]);
paresImpares(arreglo);

console.clear();


//24) Programa una función que dado un arreglo de números devuelva un objeto con dos arreglos, el primero tendrá los numeros ordenados en forma ascendente y el segundo de forma descendiente, pe. miFuncion([7, 5,7,8,6]) devolverá { asc: [5,6,7,7,8], desc: [8,7,7,6,5] }.
//25) Programa una función que dado un arreglo de elementos, elimine los duplicados, pe. miFuncion(["x", 10, "x", 2, "10", 10, true, true]) devolverá ["x", 10, 2, "10", true].
//26) Programa una función que dado un arreglo de números obtenga el promedio, pe. promedio([9,8,7,6,5,4,3,2,1,0]) devolverá 4.5.


const ordenNumeros = (arreglo = undefined) =>{

    return console.log({
        arreglo,
        asc: [...arreglo].sort((a,b)=> a - b),
        desc: [...arreglo].sort((a,b)=> b - a)
    });
}


ordenNumeros([7, 5,7,8,6]);

console.clear();


const eliminarDuplicados = (arreglo= undefined) =>{

    return console.log({
        arreglo,
        resultado: arreglo.filter((item,index)=>{
            return arreglo.indexOf(item) === index;
        })
    });
}


eliminarDuplicados(["x", 10, "x", 2, "10", 10, true, true]);

console.clear();


const promedio = (arreglo= undefined) =>{

    return console.log({
        arreglo,
        sumatoria: arreglo.reduce((acumulador,sigValor)=>{
            return acumulador + sigValor / arreglo.length;
        },0)
        
    });
}

promedio([9,8,7,6,5,4,3,2,1,0]);

console.clear();




//Programa una clase llamada Pelicula.
//La clase recibira un objeto al momento de instanciarse con los siguientes datos:
// id de la pelicula enn IMDB, titulo, director, año de estreno, pais o paises de origen,
// generos y clasificacion en IMBD.
//Todos los datos del objeto son obligatorios.
// valida  que el id IMDB tenga 9 caracteres, los primeros 2 sean letras y los 7 restantes numeros
// valida que el titulo no rebase los 100 caracteres,
//valida que el director no reabse los 50 caracteres.
//valida que el año de estreno sea un numero entero de 4 digitos.
//valida que el pais o paises sea introducido en forma de arreglo.
//valida que los generos sean introducidos en forma de arreglo.
//valida que los generos introducidos esten dentro de los generos aceptados.
//crea un metodo estatico que devuelva los generos aceptados.
//Valida que la calificaion sea un numero entre 0 y 10 pudiendo ser decimal de una posicion.
//crea un metodo que devuelva toda la ficha tecnica de la pelicula.
// a partir de un arreglo con la informacion de 3 pleiculas genera 3 instancias de la clase de forma
// autimatizada e imprime la ficha de cada pelicula.


//["Action","Adult", "Adventure", "Animation", "Biography","Comedy", "Crime", "Documentary" ,"Drama", "Family", "Fantasy", "Film Noir", "Game-Show", "History", "Horror", "Musical", "Music", "Mystery", "News", "Reality-TV", "Romance", "Sci-Fi", "Short", "Sport", "Talk-Show", "Thriller", "War", "Western"]



class Pelicula {

    constructor({id, titulo, director, estreno, paisOrigen, generos, clasificacion}){
        this.id = id;
        this.titulo = titulo;
        this.director = director;
        this.estreno = estreno;
        this.paisOrigen = paisOrigen;
        this.generos = generos;
        this.clasificacion = clasificacion;


        this.idPelicula(id);
        this.tituloPelicula(titulo);
        this.directorPelicula(director);
        this.anioEstrenoPelicula(estreno);
        this.paisPelicula(paisOrigen);
        this.generosPelicula(generos);
    }

    static get generosPelicula() {
        return [
            "Action",
            "Adult",
            "Adventure",
            "Animation",
            "Biography",
            "Comedy",
            "Crime",
            "Documentary",
            "Drama",
            "Family",
            "Fantasy",
            "Film Noir",
            "Game-Show",
            "History",
            "Horror",
            "Musical",
            "Music",
            "Mystery",
            "News",
            "Reality-TV",
            "Romance",
            "Sci-Fi",
            "Short",
            "Sport",
            "Talk-Show",
            "Thriller",
            "War",
            "Western",
        ];
    }

    static generosAceptados() {
        return console.info(`Los generos aceptados son: ${Pelicula.generosPelicula.join(", ")}`)
    }




    idPelicula(id=""){
        if(!id) return console.warn('Por favor ingrese un Id');
        if(id.length !== 9) return console.warn('favor ingrese un ID que contenga 9 caracteres');
        let arreglo = id.split('');
        if(!arreglo[0].match(/[a-z]/i) || !arreglo[1].match(/[a-z]/i)) return console.log('tiene que tener valores de letras de inicio');

        // return console.info(`Este es el Id ${id}`);
    }

    tituloPelicula(titulo=""){
        if(!titulo) return console.warn('Por favor ingrese un titulo');
        if(titulo.length > 100) return console.warn('El titulo es demasiado grande');
        if(typeof(titulo) !== 'string') return console.warn('Formato de titulo no es valido');

        // return console.info(`Este es el titulo de la pelicula ${titulo}`);
    }

    directorPelicula(director=""){
        if(!director) return console.warn('Por favor ingrese un director');
        if(director.length > 50) return console.warn('El nombre de director es demasiado grande');


        // return console.info(`Este es el titulo de la pelicula ${director}`);
    }

    anioEstrenoPelicula(fecha=undefined){
        if(fecha=== undefined) return console.warn('Por favor ingrese una fecha');
        if (fecha.toString().length !== 4) return console.warn(`cantidad de caracteres no correctos`);
        if (Number.isInteger(fecha) === false) return console.warn('Solo numeros enteros aceptados');

        // return console.info(`Esta es la fecha de estreno de la pelicua ${fecha}`);
    }


    paisPelicula(paises = undefined){
        if(!(paises instanceof Array)) return console.warn('Esto tiene un valor que no es correcto');
        if (paises.length === 0) return console.warn('Por favor ingrese datos en el array');

    }
    
    generosPelicula(genero=undefined){
        if(!(genero instanceof Array)) return console.warn('Esto no es un arreglo de generos');
        if (genero.length === 0) return console.warn('Por favor ingrese datos en el array');

        if(!genero.every(item=> Pelicula.generosPelicula.includes(item))) return console.log(`Ese genero no esta incluido en la lista`)

    }


    fichaTecnica(){
        console.info(`Ficha tecnica:\n ID de pelicula: ${this.id}\n titulo de pelicula: ${this.titulo}\n
        Director de pelicula: ${this.director}\n 
        Fecha de estreno: ${this.estreno}\n
        Paises de origen: ${this.paisOrigen.join(',')}\n
        Generos: ${this.generos.join(", ")}\n
        Clasificacion ${this.clasificacion}`);
    }
}


// const Spiderman = new Pelicula('aa321321r','Lo que el viento se llevo','steven Spielberg',1990);

let misPeliculas = [
    {
        id: "tt9376612",
        titulo: "The lord of the rings",
        director: "Peter Jackson",
        estreno: 2001,
        paisOrigen: ['Usa', 'New Zeland'],
        generos: ['Adventure', 'Mystery'],
        clasificacion: 9.54545
    },
    {
        id: "tt9376547",
        titulo: "The matrix",
        director: "Javier",
        estreno: 1999,
        paisOrigen: ['Usa'],
        generos: ['Mystery'],
        clasificacion: 9.8
    }, {
        id: "tt9374785",
        titulo: "The Mask",
        director: "Fernando",
        estreno: 1995,
        paisOrigen: ['Usa'],
        generos: ['porn', 'Film Noir'],
        clasificacion: 8.5
    }
]



misPeliculas.forEach(pelicula => new Pelicula(pelicula).fichaTecnica());