//Programa una clase llamada Pelicula.
//La clase recibira un objeto al momento de instanciarse con los siguientes datos:
// id de la pelicula enn IMDB, titulo, director, año de estreno, pais o paises de origen,
// generos y clasificacion en IMBD.
//Todos los datos del objeto son obligatorios.
// valida  que el id IMDB tenga 9 caracteres, los primeros 2 sean letras y los 7 restantes numeros
// valida que el titulo no rebase los 100 caracteres,
//valida que el director no reabse los 50 caracteres.
//valida que el año de estreno sea un numero entero de 4 digitos.
//valida que el pais o paises sea introducido en forma de arreglo.
//valida que los generos sean introducidos en forma de arreglo.
//valida que los generos introducidos esten dentro de los generos aceptados.
//crea un metodo estatico que devuelva los generos aceptados.
//Valida que la calificaion sea un numero entre 0 y 10 pudiendo ser decimal de una posicion.
//crea un metodo que devuelva toda la ficha tecnica de la pelicula.
// a partir de un arreglo con la informacion de 3 pleiculas genera 3 instancias de la clase de forma
// autimatizada e imprime la ficha de cada pelicula.


//["Action","Adult", "Adventure", "Animation", "Biography","Comedy", "Crime", "Documentary" ,"Drama", "Family", "Fantasy", "Film Noir", "Game-Show", "History", "Horror", "Musical", "Music", "Mystery", "News", "Reality-TV", "Romance", "Sci-Fi", "Short", "Sport", "Talk-Show", "Thriller", "War", "Western"]


class Pelicula {


    constructor({ id, titulo, director, estreno, paisOrigen, generos, clasificacion }) {
        this.id = id;
        this.titulo = titulo;
        this.director = director;
        this.estreno = estreno;
        this.paisOrigen = paisOrigen;
        this.generos = generos;
        this.clasificacion = clasificacion;

        this.peliculaId(id);
        this.peliculaTitulo(titulo);
        this.peliculaDirector(director);
        this.peliculaEstreno(estreno);
        this.peliculaPaisOrigen(paisOrigen);
        this.peliculaGenero(generos);
        this.peliculaCalificacion(clasificacion);
    }

    static get generosPelicula() {
        return [
            "Action",
            "Adult",
            "Adventure",
            "Animation",
            "Biography",
            "Comedy",
            "Crime",
            "Documentary",
            "Drama",
            "Family",
            "Fantasy",
            "Film Noir",
            "Game-Show",
            "History",
            "Horror",
            "Musical",
            "Music",
            "Mystery",
            "News",
            "Reality-TV",
            "Romance",
            "Sci-Fi",
            "Short",
            "Sport",
            "Talk-Show",
            "Thriller",
            "War",
            "Western",
        ];
    }

    static generosAceptados() {
        return console.info(`Los generos aceptados son: ${Pelicula.generosPelicula.join(", ")}`)
    }


    peliculaId(id = "") {
        id = id.toLocaleLowerCase();
        if (!id) return console.log('por favor ingrese un id de pelicula!');
        if (id.length !== 9) return console.log(`El id ingresado ${id} no tiene los 9 caracteres necesarios`);
        if (!id.charAt([0, 1]).match(/[a-z]/i)) return console.log(`El formato que intentas ingresar no es correcto`);
    }

    peliculaTitulo(titulo = "") {
        if (!titulo) return console.warn('Por favor ingresa un titulo');
        if (typeof titulo !== "string") return console.warn(`El formato introducido ${titulo} no es valido`);
        if (titulo.length > 100) return console.warn('El titulo es demasiado largo');
    }

    peliculaDirector(director = "") {
        if (!director) return console.warn('Por favor ingresa un nombre de director');
        if (typeof director !== "string") return console.warn(`El formato introducido ${director} no es valido`);
        if (director.length > 50) return console.warn('El nombre es demasiado largo');
    }

    peliculaEstreno(anio = undefined) {
        if (!anio) return console.warn('Por favor ingresa la fecha de estreno');
        if (typeof anio !== "number") return console.warn(`El formato introducido ${anio} no es valido`);
        if (anio.toString().length !== 4) return console.warn(`cantidad de caracteres no correctos`);
        if (Number.isInteger(anio) === false) return console.warn('Solo numeros enteros aceptados');
    }

    peliculaPaisOrigen(pais) {
        if (!(pais instanceof Array)) return console.warn('Este formato no es aceptado, debe ser un array');
        if (pais.length === 0) return console.warn('Por favor ingrese datos en el array');
        for (let cadena of pais) {
            if (typeof cadena !== "string") return console.error(`El valor de ${cadena} ingresado no es un string`);
        }
    }

    peliculaGenero(genero = undefined) {
        if (!(genero instanceof Array)) return console.warn('Este formato no es aceptado, debe ser un array');
        if (genero.length === 0) return console.warn('Por favor ingrese datos en el array');
        if (!genero.every(item => Pelicula.generosPelicula.includes(item))) return console.log(`Ese genero no esta permitido, los generos permitidos son ${Pelicula.generosAceptados()}`);
    }

    peliculaCalificacion(calificacion) {
        if (!calificacion) return console.warn('Por favor ingrese una calificacion!');
        if (Math.sign(calificacion) === -1 || (calificacion > 10)) return console.warn('ingrese un numero valido, que sea entre 1 a 10');
    }

    fichaTecnica() {
        console.info(`Ficha tecnica:\n ID de pelicula: ${this.id}\n titulo de pelicula: ${this.titulo}\n
        Director de pelicula: ${this.director}\n 
        Fecha de estreno: ${this.estreno}\n
        Paises de origen: ${this.paisOrigen.join(", ")}\n
        Generos: ${this.generos.join(", ")}\n
        Clasificacion ${this.clasificacion}`);
    }
}

// const theRing = new Pelicula({
//     id: "tt9376612",
//     titulo: "The lord of the rings",
//     director: "Peter Jackson",
//     estreno: 2001,
//     paisOrigen: ['Usa', 'New Zeland'],
//     generos: ['Adventure', 'Mystery'],
//     clasificacion: 9.54545
// });

// theRing.fichaTecnica();

let misPeliculas = [
    {
        id: "tt9376612",
        titulo: "The lord of the rings",
        director: "Peter Jackson",
        estreno: 2001,
        paisOrigen: ['Usa', 'New Zeland'],
        generos: ['Adventure', 'Mystery'],
        clasificacion: 9.54545
    },
    {
        id: "tt9376547",
        titulo: "The matrix",
        director: "Javier",
        estreno: 1999,
        paisOrigen: ['Usa'],
        generos: ['Mystery'],
        clasificacion: 9.8
    }, {
        id: "tt9374785",
        titulo: "The Mask",
        director: "Fernando",
        estreno: 1995,
        paisOrigen: ['Usa'],
        generos: ['Adventure', 'Film Noir'],
        clasificacion: 8.5
    },
]

misPeliculas.forEach(pelicula => new Pelicula(pelicula).fichaTecnica());

