//funcion que invierte a sentido contrario las palabras de una cadena de texto("Hola mundo");

let invertirSentido = (palabra = undefined) => {

    (!palabra || (palabra.length <= 4))
        ? console.warn('Es una palabra invalida')
        : console.log(`Es una palabra valida ${palabra.split("").reverse().map(w => w[0].toUpperCase() + w.substring(1).toLowerCase()).join("")}`)
}
// invertirSentido('Maria Gabriela');
// invertirSentido('Fernando Javier');


//Ejercicio 6 Programa una funcion para contar el numero de veces que se repite una palabra en un texto largo.


let detectarPalabra = (frase = undefined, palabra = undefined) => {
    ((!frase && !palabra) || (!frase || !palabra))
        ? console.warn('Por favor ingrese los datos')
        : (frase.indexOf(palabra) === -1)
            ? console.log('No existe la palabra que buscas')
            : console.log(`Esta palabra se repite ${frase.indexOf(palabra)}`)

}

// detectarPalabra('ayer dia ha sido un dia', 'dia');

let textoEnCadena = (cadenas = "", texto = "") => {
    if (!cadenas) {
        return console.warn("No ingresaste el texto");
    }
    if (!texto) return console.warn("No ingresaste palabra a aevaluar");

    let i = 0;
    let contador = 0;

    while (i !== -1) {
        i = cadenas.indexOf(texto, i);
        if (i !== -1) {
            i++;
            contador++;
        }
    }

    return console.log(`La palabra ${texto} se repite ${contador} veces`);

}

// textoEnCadena("hola mundo adios mundo", "mundo");


//Ejercicio 7 Una funcion que valide si una palabra o frase, es un palindromo(palabra: salas = true)

let palindromo = (palabra = "") => {
    palabra = palabra.toLowerCase()
    if (!palabra) return console.warn('Por favor inserte una palabra');
    let palabraReversed = palabra.split("").reverse().join("");

    return (palabraReversed === palabra) ? "Es palindromo" : "No es palindromo"
}


// console.log(palindromo("Salas"))

// palindromo();

//Ejercicio 8 Una funcion que elimine cierto patron de caracteres de un texto dado.

let patron = (cadena = "", codigo = "") => {

    if (!cadena || !codigo) return console.log('Por favor ambos valores');
    let remplazo = cadena.replace(new RegExp(codigo, "ig"), "");
    return console.log(remplazo);
}

// patron("xyz1,xyz2,xyz3,xyz4,xyz5", "xyz");
// patron("xyz1,xyz2,xyz3,xyz4,xyz5", "[a-z]");

// Ejercicio 9 , un programa que obtenga un numero aleatorio entre 501 y 600

let numeroAleatorio = (num1 = undefined, num2 = undefined) => {
    if (!num1 || !num2) return console.log('Por favor ingrese todos los datos');
    return console.log(Math.round(Math.random() * (num2 - num1)) + num1);


}
// numeroAleatorio(501,600);

//Ejercicio 10 Funcion que reciba un numero y evalue si es capicua o no(Que se lea igual de las dos formas);


let numeroCapicua = (numero = undefined) => {
    if (!numero) return console.log('Por favor ingrese un numero');
    if (typeof numero !== "number") return console.warn("Ingrese un valor numero correcto");
    let numeroAlreves = numero.toString().split("").reverse().join("");

    (numero.toString() === numeroAlreves)
        ? console.log('Es un numero capicua')
        : console.log('No es numero capicua')

}

// numeroCapicua("2002");


// Ejercicio 11 , calcular una funcion que se le de un numero y calcule el factorial.

let factorial = (numero = undefined) => {
    if (numero === undefined) return console.warn("No ingresaste el numero");
    if (typeof numero !== "number") return console.warn("Ingresa un valor valido");
    if (numero === 0) return console.error("No se puede usar 0");
    if (Math.sign(numero) === -1) return console.error("El numero no puede ser negativo");

    let factorial = 1;
    for (let i = numero; i > 1; i--) {
        factorial = factorial * i;
    }

    return console.info(`El factorial de ${numero} es ${factorial}`)
}


// factorial("0");

//ejercicio 12 Una funcion que deermine si un numero es primo, aquel que solo es divisible por si mismo,

let numeroPrimo = (numero = undefined) => {
    if (numero === undefined) return console.warn("No ingresaste el numero");
    if (typeof numero !== "number") return console.warn("Ingresa un valor valido");
    if (numero === 0) return console.error("No se puede usar 0");
    if (numero === 1) return console.error("No se puede usar 1");
    if (Math.sign(numero) === -1) return console.error("El numero no puede ser negativo");

    let divisible = false;

    for (let i = 2; i < numero; i++) {
        if ((numero % i) === 0) {
            divisible = true;
            break;
        }
    }
    return (divisible)
        ? console.log('El numero no es primo')
        : console.log('El numero si es primo')

}

// numeroPrimo(5);

// ejercicio 13, un programa que determine si es par o impar.

let numeroImpar = (numero = undefined) => {
    if (numero === undefined) return console.warn("No ingresaste el numero");
    if (typeof numero !== "number") return console.warn("Ingresa un valor valido");
    if (numero === 0) return console.error("No se puede usar 0");
    if (numero === 1) return console.error("No se puede usar 1");
    if (Math.sign(numero) === -1) return console.error("El numero no puede ser negativo");

    (numero % 2 === 0)
        ? console.log("es numero par")
        : console.log("No es numero par")
}

// numeroImpar(29);

//ejercicio 14 programa para convertir grados celsios a fahrenheit

let temperatura = (grados = undefined, tipo = undefined) => {
    if (tipo === undefined) return console.log("No ingresaste el tipo de grado")
    if (typeof tipo !== "string") return console.log("Ingresa un valor valido");
    tipo = tipo.toLowerCase();
    if (grados === undefined) return console.log("No ingresaste los grados");
    if (typeof grados !== "number") return console.log("Ingresa un numero valido");



    (tipo === "c")
        ? console.log(Math.round((grados * 9 / 5) + 32))
        : console.log(Math.round((grados - 32) * 5 / 9))



}
// temperatura(32, "f");


// ejercicio 15 programa funcion para convertir los numeros de base binaria a decimal y vicecersa.

let convertidorNum = (numero = undefined, base = undefined) => {
    if (numero === undefined) return console.log('Por favor ingrese los valores');
    if (typeof numero !== "number") return console.log('El formato de numero es incorrecto');

    if (base === undefined) return console.log('Por favor ingrese los valores');
    if (typeof base !== "number") return console.log('El formato de numero es incorrecto');

    if (base === 2) {
        return console.log(parseInt(numero, base));
    } else if (base === 10) {
        return console.log(numero.toString(base));
    } else {
        return console.log('El formato que has introducido no es valido');
    }

}

// convertidorNum(6,3);

//Ejercicio 16 programa una funcion que devuelva el monto final despues de aplicar un descuento a una cantidad dada,


let descuento = (cantidad = undefined, descuento = 0) => {
    if (cantidad === undefined) return console.log('Por favor ingrese la cantiadad');
    if (typeof cantidad !== "number") return console.log('El formato de numero es incorrecto');
    if (cantidad === 0) return console.log('La cantidad no puede ser 0');
    if (Math.sign(cantidad === -1)) return console.log('No puedes ingresar valores negativos');

    if (descuento === 0) return console.log('Por favor ingrese la cantidad');
    if (typeof descuento !== "number") return console.log('El formato de numero es incorrecto');
    if (Math.sign(descuento === -1)) return console.log('No puedes ingresar valores negativos');

    // if (descuento >= cantidad) return console.log('El descuento no pueden ser mayor que la cantidad');

    let descuentoFinal = ((cantidad / 100) * descuento);
    return console.log(`el precio ya con el descuento ha sido de ${cantidad - descuentoFinal} y el descuento ha sido de ${descuentoFinal} euro`);
}


// descuento(10, -25);



//Tarea 17 programa una funcion que dada una fecha valida determine cuantos dias han pasado hasta el dia de hoy


let calcularAnios = (fecha = undefined) => {
    if (fecha === undefined) return console.warn("no ingresaste la fecha");

    if (!(fecha instanceof Date)) return console.log('El valor que ingresaste no es una fecha valida');

    let hoyMenosFecha = new Date().getTime() - fecha.getTime();
    let aniosEnMs = 1000 * 60 * 60 * 24 * 365;
    let aniosHumanos = Math.floor(hoyMenosFecha / aniosEnMs);

    return (Math.sign(aniosHumanos) === -1)
        ? console.log(`Faltan ${Math.abs(aniosHumanos)} años para el ${fecha.getFullYear()}`)
        : (Math.sign(aniosHumanos) === 1)
            ? console.log(`Han pasado ${aniosHumanos} años para el ${fecha.getFullYear()}`)
            : console.log(`Estamos en el año actual ${fecha.getFullYear()}`)
}


// calcularAnios({});
// calcularAnios(new Date());
// calcularAnios(new Date(1890, 1, 3));

// tarea 18 Programa una funcion dad una cadena de texto cuente el numero de vocales y consolantes
// miFuncion("Hola mundo") devuelve  Vocales 4 , Consonantes 5

let contador = (frase = "") => {
    frase = frase.toLocaleLowerCase();
    if (!frase) return console.log('Por favor ingrese una palabra o frase');
    if (typeof frase !== "string") return console.log('Por favor ingrese un formato valido');
    if (frase.length <= 2) return console.log('ingrese una palabra o frase mas larga por favor');

    let numeroVocales = frase.match(/[aeiou]/gi).length;
    let numeroConsonantes = frase.match(/[bcdfghjklmnñpqrstvwxyz]/gi).length;
    return console.log(`tiene estas vocales ${numeroVocales} y tiene estas consonantes ${numeroConsonantes}`);
}
// contador('Hola mundo');
// contador('fernando javier ramirez obando');
// contador('ma');




//tarea 19 Programa una funcion que valide un texto sea un nombre valido, miFuncion("Fernando Javier")
//devolvera true

let validarTexto = (frase = "") => {
    frase = frase.toLocaleLowerCase().replace(/ /g, "");
    if (!frase) return console.warn('Por favor ingrese el valor');
    if (typeof frase !== "string") return console.log('Por favor ingrese un formato valido');

    nomUsuario = "fernandojavier";

    return console.log(nomUsuario.includes(frase));
}

// validarTexto('fernando Javier');

//ejercicio 20 programa una funcion que valide que email tenga el formato correcto.


let validarEmail = (email = "") => {
    if (!email) return console.log('Ingrese por favor un mail');
    if (typeof email !== "string") return console.log("Esto es un formato incorrecto");

    let expReg = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;

    (expReg.test(email))
        ? console.log('Mail valido')
        : console.log('Mail Invalido')

}
// validarEmail('ferjavi@gmail.com');


// Tarea 21, programa una funcion que dado un array numerico devuelve otro array con los numeros elevados al cuadrado
// ejemplo miFuncion([1,4,5]) devolvera [1,16,25]

let arra1 = [1, 4, 5];

let numElevados = (arreglo = undefined) => {
    if (arreglo === undefined) return console.warn('No ingresaste el arreglo de numeros');
    if (!(arreglo instanceof Array)) return console.error('El valor que ingresaste no es un arreglo');
    if (arreglo.length === 0) return console.log('El arreglo esta vacio');
    for (let num of arreglo) {
        if (typeof num !== "number") return console.error(`El valor ${num} ingresado no es un numero`);
    }
    let potencia = arreglo.map((num) => {
        return (num ** 2);
    });
    return potencia;
}

// numElevados(["2",3,4]);
// let numerosPotencia = numElevados(arra1);
// console.log(numerosPotencia, arra1);


//Programa una funcion que dado un array devuelva el numero mas alto y el mas bajo,
// miFuncion([1,4,5,99,-60]) devolvera [99,-60]

let numMinMax = (arreglo = undefined) => {
    if (!arreglo) return console.log('Por favor ingrese un arreglo');
    if (!(arreglo instanceof Array)) return console.error('El valor que ingresaste no es un arreglo');
    if (arreglo.length === 0) return console.log('El arreglo esta vacio');
    for (let num of arreglo) {
        if (typeof num !== "number") return console.error(`El valor ${num} ingresado no es un numero`);
    }
    // let numMax = Math.max(...arreglo);
    // let numMin = Math.min(...arreglo);
    // let numeros = [numMax, numMin];
    // return console.log(numeros);
    return console.info(`el arreglo original es ${arreglo}\n el numero minimo es ${Math.min(...arreglo)} y el
     numero maximo es ${Math.max(...arreglo)}`);
}

// numMinMax(["3", 4, 3, 2])
// numMinMax([1, 4, 5, 99, -60]);



//Uso del spread operator
const perro = {
    nombre: 'ulrika',
    apellido: 'ramirez'
}

const perro2 = { ...perro };
perro2.nombre = 'Bambi';

let perros = [perro, perro2];


const ferNum = [1, 2, 3, 4];

const ferNum2 = [...ferNum];

ferNum2.push(5);
// console.log(ferNum)
// console.log(ferNum2);

// console.log(perros[0], perros[1]);

//Tarea 23 Programa una funcion que dado un array de numeros devuelva un objeto con dos arreglos
//en el primero almacena pares y en el segundo impares ejm miFuncion([1,2,3,4,5,6,7,8,9,0])
// y esto va a devolver {pares: [2,4,6,8], impares [1,3,5,7,9]}


let paresEimpares = (arreglo = undefined) => {
    if (!arreglo) return console.log('Por favor ingrese un array');
    if (!(arreglo instanceof Array)) return console.log('Por favor ingrese array valido');
    for (let num of arreglo) {
        if (typeof num !== "number") {
            return console.log(`Este caracter no es de tipo numero ${num}`);
        }
    }
    return console.info({
        pares: arreglo.filter(num => num % 2 === 0),
        impares: arreglo.filter(num => num % 2 === 1)
    })
}

// paresEimpares([1, 2, 3, 4, 5, 6, 7, 8, 9, 0]);


//tarea 24 , has una funcion que dado un arreglo de numeros devuelva un objeto con 2 arreglos
// el primero tendra los numeros ordenados en forma ascendente y el segundo de forma descendiente
// miFuncion([7,5,8,6]) devolvera {asc: [5,6,7,7,8], des: [8,7,7,6,5]}


let numOrdernar = (arreglo = undefined) => {
    if (!arreglo) return console.log('Por favor ingrese un array');
    if (!(arreglo instanceof Array)) return console.log('Por favor ingrese array valido');
    for (let num of arreglo) {
        if (typeof num !== "number") {
            return console.log(`Este caracter no es de tipo numero ${num}`);
        }
    }
    // return console.info({
    //     asc: [...arreglo].sort((a, b) => a - b),
    //     desc: [...arreglo].sort((a, b) => b - a)
    // });

    return console.info({
        arreglo,
        desc: arreglo.map(num => num).sort(),
        asc: arreglo.map(num => num).sort().reverse()
    })
}
// numOrdernar([7, 5, 7, 8, 6]);


// Ejercicio 25 , una funcion que dado un arreglo de elemenos elimine los duplicados. 
// ejemplo miFuncion(["x",10,"x",2,"10",10, true, true])

let eliminarRepetidos = (arreglo = undefined) => {
    if (!arreglo) return console.log('Por favor ingrese un array');
    if (!(arreglo instanceof Array)) return console.log('Por favor ingrese array valido');
    if (arreglo.length === 0) return console.log('El arreglo esta vacio');
    if (arreglo.length === 1) return console.log('El arreglo tiene que tener mas de 1 elemento');

    //Aqui podemos ver dos formas de eliminar duplicados de un array, usando reduce o Set
    return console.info({
        original: arreglo,
        resultado: arreglo.reduce((acc, item) => {
            if (!acc.includes(item)) {
                acc.push(item);
            }
            return acc;
        }, []),
        sinDuplicados: [...new Set(arreglo)]
    });
}

// eliminarRepetidos(["x", 10, "x", 2, "10", 10, true, true]);

// 26 Programa una funcion que dado un arreglo de numeros obtenga el promedio ,
// promedio([9,8,7,6,5,4,3,2,1,0]) return 4.5,


let calcularPromedio = (arreglo = undefined) => {
    if (!arreglo) return console.log('Por favor ingrese un array');
    if (!(arreglo instanceof Array)) return console.log('Por favor ingrese array valido');
    for (let num of arreglo) {
        if (typeof num !== "number") {
            return console.log(`Este caracter no es de tipo numero ${num}`);
        }
    }
    // let suma = arreglo.reduce((total, num) => num += total);
    // let promedio = suma / arreglo.length;
    // return console.log(`el promedio de este arreglo ${arreglo} es de ${promedio}`);

    return console.info(
        arreglo.reduce((total,num, index, arreglo)=>{
            total += num;
            if(index === arreglo.length -1){
                return `El promedio de ${arreglo.join("+")} es de ${total/arreglo.length}`
            }else{
                return total;
            }
        })
    )
}

// calcularPromedio({})
// calcularPromedio([9, 8, 7, 6, 5, 4, 3, 2, 1, 0]);

