class Ejercicios1{
    constructor(frase,caracteresCortar,oracion,cadenaMulti){
        this.frase = frase;
        this.caracteresCortar = caracteresCortar;
        this.oracion = oracion;
        this.cadenaMulti = cadenaMulti;
    }

    contarNumeros(){
        if(!this.frase) return console.log('Por favor ingrese una frase');
        if(typeof(this.frase)!== 'string') return console.warn(`Esto ${this.frase} tiene que ser un string`);
        return console.log(this.frase.length);
    }
    recortartexto(){
        if(typeof(this.frase)!== 'string') return console.warn(`Esto ${this.frase} tiene que ser un string`);
        if(typeof(this.caracteresCortar)!== 'number') return console.warn(`Esto ${this.frase} tiene que ser un numero`);
        if(this.frase.length < this.caracteresCortar) return console.warn(`El numero ${this.caracteresCortar} sobrepasa el largo de la frase ${this.frase.length}`);
        let textoCortado = this.frase.slice(0,this.caracteresCortar);
        return console.log(textoCortado);
    }

    cadenaToArreglo(){
        if(typeof(this.oracion)!== 'string') return console.warn(`Esto ${this.oracion} tiene que ser un string`);
        return console.log(this.oracion.split(' '));
    }

    cadenaMultiplicar(xVeces=null){
        if(typeof(this.cadenaMulti)!== 'string') return console.warn(`Esto ${this.cadenaMulti} tiene que ser un string`);
        if(xVeces=== null) return console.warn(`Esto ${xVeces} tiene que tener un valor`);
        return console.log(this.cadenaMulti.repeat(xVeces));
    }
}



const tarea1 = new Ejercicios1('fernando',3, 'Hola que tal',' Hola mundo');
const tarea2 = new Ejercicios1('ulrika',10,'mañana nos vamos', ' Hola marte');


tarea1.contarNumeros();
tarea1.recortartexto();
tarea1.cadenaToArreglo();
tarea1.cadenaMultiplicar(5);
console.clear();
tarea2.cadenaMultiplicar(10);


console.clear();



const recortar = (cadena="",longitud=undefined) =>{
    if(!longitud) return console.log('Por favor ingresa un valor');
    if(cadena.length<=longitud) return console.warn(`el valor que has introducido no es valido ${cadena}`);
    if(typeof(cadena)!=='string') return console.warn(`Formato no aceptado ${typeof(cadena)}`);
    if(typeof(longitud)!=='number') return console.log(`Solo se acepta formato numero ${longitud}`);
    return console.log(cadena.slice(0,longitud));

}


recortar('fernando',3);