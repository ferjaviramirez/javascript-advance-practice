export const PI = Math.PI;

export let usuario = "Fernando";
let password = "qwerty";

export default function saludar() {
console.log('Hola Modulos en ES6');
}

export class Saludar {
    constructor(nombre){
        this.nombre = nombre;
    }

    despedirse(){
        console.log('me voy mañana');
    }

    decirNombre(vecino){
        let expReg = /ulrika/g;
        if(expReg.test(vecino.toLowerCase())===false) return console.info(`Nombre de vecino ${vecino} no es conocido`);
        if(typeof(vecino) !== 'string') return console.warn('Nombre de vecino no valido');
        setTimeout(()=>{
            return console.log(`Este es tu nombre ${this.nombre} y este el nombre de tu vecino ${vecino}`);
        },2000);      
    }
}