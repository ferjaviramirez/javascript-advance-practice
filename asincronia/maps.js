//Este es un tipo de dato que se parece a un Objeto.

let mapa = new Map();

mapa.set('nombre','Fernando');
mapa.set('edad','31');
mapa.set('direccion','calle de los riscos 40');

console.log(mapa);
console.log(mapa.has('edad'));
console.log(mapa.set('edad','51'));
console.log(mapa.get('edad'));
console.log(mapa.size);

console.clear();


//aqui hacemos la desestructuracion para obtener los valores:
for(let [key, value] of mapa){
    console.log(key,value);

}

console.clear();

//Esta es otra forma de hacer un Map:

const mapa2 = new Map([
    ["nombre","Ulrika"],
    ["edad",7],
    ["animal", "perro"]
]);

console.log(mapa2);

// Esot aqui vamos a almacenar las llaves y los valores separados:

let llavesMapa = [...mapa2.keys()];
let valoresMapa = [...mapa2.values()];


console.log(`estos son las llaves ${llavesMapa} y esto los valores ${valoresMapa}`);