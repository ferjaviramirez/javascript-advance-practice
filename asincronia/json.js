//JSON es la forma que un lenguage de programacion se comunica con otro lenguaje, por ejemplo comunicarse PHP con PYTHON, se necesita que los datos que van enviar
// sean en JSON.


//Convertir de JSON a lenguaje para JAVASCRIPT se usa JSON.parse();

console.log(JSON.parse("true"));
console.log(JSON.parse("false"));
// console.log(JSON.parse("hola mundo")); en este caso podemos ver que est string no esta soportado.
console.log(JSON.parse("[]"));
console.log(JSON.parse("{}"));




//Ahora vamos a ver el caso contrario que seria de lenguage JAVASCRIPT a JSON y se usa JSON.stringify(), esto los convierte en cadena de texto:
console.log('**************************** stringify *************************************');
console.log(JSON.stringify({}));
console.log(JSON.stringify(true));
console.log(JSON.stringify(false));
console.log(JSON.stringify([1,2,3,4,5,6]));
console.log(JSON.stringify({y:2,x:3}));