// Es cuando una funcion se va a ejcutar luego de que otra lo haga.

// function cuadradoCallback(value, callback) {
//     setTimeout(() => {
//         callback(value, value * value)
//     }, 0 | Math.random() * 1000)
// }


// cuadradoCallback(1, (value, result) => {
//     console.log('Inicia Calback');
//     console.log(`Callback: ${value}, ${result}`);
//     cuadradoCallback(2, (value, result) => {
//         console.log(`Callback: ${value}, ${result}`);
//         cuadradoCallback(3, (value, result) => {
//             console.log(`Callback: ${value}, ${result}`);
//         });
//     });
// });



// console.log('start');

// function loginUser(mail, password, callback) {

//     setTimeout(() => {
//         callback({ userName: mail, contra: password })
//     }, 1000);
// }

// function getUserVideos(email, callback) {
//     setTimeout(() => {
//         callback(['video1', 'video2']);
//     }, 2000);
// }

// function videoDetails(video, callback) {
//     setTimeout(() => {
//         callback('Title of the video');
//     }, 3000)
// }

// function actorMovie(actor, callback) {
//     setTimeout(() => {
//         callback(['Dicaprio', 'Brad Pitt'])
//     }, 4000)
// }

// let usuario = loginUser('ferjaviramire@gmail.com', 123456, (user) => {
//     console.log(user)
//     getUserVideos(user.userName, (userVideos) => {
//         console.log(userVideos);
//         videoDetails(userVideos[1], (detail) => {
//             console.log(detail);
//             actorMovie(detail, (actor) => {
//                 return (actor.forEach((act)=>{
//                     console.log(`Este es el actor ${act}`)
//                 }));
//             })
//         })
//     });
// });
// console.log('finish');

// function cuadradoCallback(value, callback){
//     setTimeout(()=>{
//         callback(value, value * value);

//     },0 | Math.random() * 1000);
// }

//Esto es un callback hell
// cuadradoCallback(1, (value, result)=>{ 
//     console.log('incia el callback');
//     console.log(`callback: ${value}  y resultado ${result}`);
//     cuadradoCallback(2, (value, result)=>{ 
//         console.log('segundo callback');
//         console.log(`callback: ${value}  y resultado ${result}`);
//         cuadradoCallback(3, (value, result)=>{ 
//             console.log('tercer callback');
//             console.log(`callback: ${value}  y resultado ${result}`);
//             cuadradoCallback(4, (value, result)=>{ 
//                 console.log('cuarto callback');
//                 console.log(`callback: ${value}  y resultado ${result}`);
//             });
//         });
//     });
// });


function cuadradoCallback(value,callback){
    setTimeout(()=>{
        callback(value, value * value);
    },0 | Math.random() *100);
}

cuadradoCallback(0, (value,result)=>{
    console.log('Inica callback');
    console.log(`callack ${value} resultado ${result}`);
    cuadradoCallback(1, (value,result)=>{
        console.log('Inica callback');
        console.log(`callack ${value} resultado ${result}`);
    });
    cuadradoCallback(2, (value,result)=>{
        console.log('Inica callback');
        console.log(`callack ${value} resultado ${result}`);
    });
    cuadradoCallback(3, (value,result)=>{
        console.log('Inica callback');
        console.log(`callack ${value} resultado ${result}`);
    });
});