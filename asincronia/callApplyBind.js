

console.log(this);
this.lugar = 'Contexto global';


function saludar(saludo, aQuien){
    console.log(`${saludo} de parte de ${aQuien} desde este lugar ${this.lugar}`);
}

saludar('que tal','javier');

const Obj = {
    lugar: 'Contexto de objeto'
}

//Como podemos ver aqui el metodo call esta llamando al this.lugar del objeto que estamos metiendo como argumento. EN ESTE METEMOS ARGUEMENTOS COMO STRINGS
saludar.call(Obj,'Hola','fernando');

//como podemos ver en el metodo, aqui se hace el llamo al this igual que con call pero en este caso la diferencia es que se mete los arguementos como un arreglo 
saludar.apply(Obj,['buenas','mario']);

console.clear();



const persona = {
    nombre: 'Javier',
    saludar: function(){
        console.log(`Hola ${this.nombre}`);
    }
}


persona.saludar();

//si quitamos el metodo bind y solo intentamos llamar la funcion saludar va a dar undefined porque el objeto otra persona no tiene la propiedad nombre que usa el objeto persona en la linea 27
const otraPersona = {
    saludar: persona.saludar.bind(persona)
}


otraPersona.saludar();