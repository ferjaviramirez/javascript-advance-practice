

const iterable = [1, 2, 3, 4, 5, 6];

const iterador = iterable[Symbol.iterator]();

console.log(iterador);

// console.log(iterador.next());
// console.log(iterador.next());
// console.log(iterador.next());
// console.log(iterador.next());
// console.log(iterador.next());
// console.log(iterador.next());
// console.log(iterador.next());

let next = iterador.next();


while (!next.done) {
    console.log(next.value);
    next = iterador.next();
}

console.clear();


//*************************************************************generators */
// Esta es la forma mas amigable de trabajar con iteradores: tenemos que agregar el asterisco a la funcion.


function* iterable2() {
    yield "hola";
    console.log('Hola consola');
    yield "hola 2";
    console.log("Seguimos con mas instrucciones de nuestro codigo");
    yield "hola3";
    yield "hola4";
}


let iterador2 = iterable2();
// console.log(iterador2.next());
// console.log(iterador2.next());
// console.log(iterador2.next());
// console.log(iterador2.next());
// console.log(iterador2.next());

for (let y of iterador2) {
    console.log(y);
}

const arreglo = [...iterable2()];

console.log(arreglo);

console.clear();


//aqui vamos a ver una asincronia usando el yield que va ser no bloqueante: 

function cuadrado(valor) {
    setTimeout(() => {
        return console.log({
            valor: valor,
            resultado: valor * valor
        })

    }, Math.random() * 1000);

}



function* generador(){
    console.log('Inicia Generator');
    yield cuadrado(0);
    yield cuadrado(1);
    yield cuadrado(2);
    yield cuadrado(3);
    yield cuadrado(4);
    yield cuadrado(5);
    console.log('Termina Generator');
}


let gen = generador();

for(let y of gen){
    console.log(y);
}