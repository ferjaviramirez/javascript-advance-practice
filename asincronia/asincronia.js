// setTimeout(() => {
//     console.log('Ejecutando un setTimeOut, se ejecuta una sola vez')
// }, 1000);


// setInterval(() => {
//     console.log('Ejecutando un setInterval, esto se ejecuta indefinidamente cada cierto intervalo de tiempo')
// }, 1000)

// let temporizador = setTimeout(()=>{
//     console.log(new Date().toLocaleTimeString());
// },1000);
//con el clear time out controlamos el setTimeOut
// clearTimeout(temporizador);

// let intervalos = setInterval(()=>{
//     console.log(new Date().toLocaleTimeString());
// },1000);

//con el clear interval limpiamos los intervalos
// setTimeout(()=>{
//     clearInterval(intervalos);
// },3000);


// function callback() {
//     console.log('Esto es un callback');
// }

// function saludar(fn) {
//     fn();
// }


// // saludar(callback);

// function mensaje1(fn) {
//     setTimeout(() => {
//         console.log('Este es primer mensaje');
//         fn();
//     }, 2000)
// }

// function mensaje2() {
//     console.log('Este es segundo mensaje');
// }


// mensaje1(mensaje2);


//Codigo sincrono bloqueante

// (() => {
//     console.log("Codigo Sincrono");
//     console.log("Inicio");

//     function dos(){
//         console.log("Dos");
//     }

//     function uno(){
//         console.log("Uno");
//         dos();
//         console.log("Tres");
//     }

//     uno();
//     console.log("Fin");

// })();

console.log("***********************************************************");
//Codigo asincrono bloequeante

(() => {

    console.log("Codigo asincrino");
    console.log("Inicio");

    function dos() {
        setTimeout(() => {
            console.log("Dos");
        }, 1000);
    }

    function uno() {
        setTimeout(() => {
            console.log("Uno");
        }, 0);
        dos();
        console.log("Tres");
    }

    uno();
    console.log("Fin");
})();

