//Un set es un tipo de dato que solo valora tipos de datos unicos, es parecido a un arreglo pero mejorado.

const set = new Set([1,2,3,3,3,4,5,6,true, true,{},{},'hola', "hOla"])

console.log(set);


const set2 = new Set();

set2.add(1);
set2.add(2);
set2.add(2);
set2.add(3);

console.log(set2);

for(let item of set2){
    console.log(item);
}


console.log('********************* Recorriendo set ***************************');


set2.forEach((item)=> console.log(item));


//para convertir un objeto Set en un iterable hacemos esto:

//Esto a primeras nos va a dar undefined, es por eso que debemos convertirlo:
console.log(set2[0]);

//almacenamos en una nueva variable el Set para que asi se pueda covertir,
let arreglo = Array.from(set2);

console.log(arreglo[0]);

//tambien los set tienen otros metodos por ejemplo:
set2.delete(3);
console.log(set2);

console.clear();
//esto es para comprobar si un elemento existe en el set, devolvera true or false
console.log(set2.has(2));

