//las promesas, trabajan con dos recursos principales.result, reject


// function cuadradoPromise(value) {
//     if (typeof value !== "number") {
//         return Promise.reject(`Error este dato ${value} no es de tipo numero`);
//     }
//     return new Promise((resolve, reject) => {
//         setTimeout(() => {
//             resolve({
//                 value: value,
//                 result: value * value
//             });
//         }, 0 | Math.random() * 1000)

//     });
// }
// cuadradoPromise(true)
//     .then((valor) => {
//         console.log(valor)
//     })
//     .catch((err) => {
//         console.log(err)
//     })




// const promise = new Promise((resolve, reject) => {

//     setTimeout(() => {
//         console.log('Got the user');
//         reject(new Error('this user is not allowed to get into base data!'));
//     }, 2000);

// });


// promise
//     .then(user => {
//         console.log(user);
//     })
//     .catch((err) => {
//         console.log(err.message)
//     })

// function cuadradoCallback(value, callback){
//     setTimeout(()=>{
//         callback(value, value * value);

//     },0 | Math.random() * 1000);
// }


// function cuadradoPromise(value){
//     return new Promise((resolve, reject)=>{
//         setTimeout(()=>{
//             resolve({
//                 value: value,
//                 result: value * value
//             })
//         },0 | Math.random() * 1000);
//     });
// }

// cuadradoPromise(5)
//     .then((obj)=>{
//         console.log('inicio de promise');
//         console.log(`Promise ${obj.value} y su resultado es ${obj.result}`);
//         return cuadradoPromise(6);
//     })
//     .then((obj)=>{
//         console.log('segunda promsesa');
//         console.log(`Promise ${obj.value} y su resultado es ${obj.result}`);
//         return cuadradoPromise(7);
//     })
//     .then((obj)=>{
//         console.log('segunda promsesa');
//         console.log(`Promise ${obj.value} y su resultado es ${obj.result}`);
//         return cuadradoPromise(8);
//     })
//     .then((obj)=>{
//         console.log('segunda promsesa');
//         console.log(`Promise ${obj.value} y su resultado es ${obj.result}`);
//         return cuadradoPromise(9);
//     })
//     .catch();


function cuadradoPromise(value) {
    if(typeof value !== 'number') return Promise.reject(`El valor ingresado ${value} no es un numero`);
    return new Promise((resolve, reject)=>{
        setTimeout(() => {
            resolve({
                value:value,
                result: value * value
            })
    
        }, 0 | Math.random() * 1000);
    });
}


cuadradoPromise(0)
.then((obj)=>{
    console.log('Inicio de la primera promesa');
    console.log(`Promesa ${obj.value} y ${obj.result}`);
    return cuadradoPromise(1);
})
.then((obj)=>{
    console.log('Inicio de la segunda promesa');
    console.log(`Promesa ${obj.value} y ${obj.result}`);
    return cuadradoPromise(2);
})
.then((obj)=>{
    console.log('Inicio de la tercera promesa');
    console.log(`Promesa ${obj.value} y ${obj.result}`);
    return cuadradoPromise(3);
})
.then((obj)=>{
    console.log('Inicio de la cuarta promesa');
    console.log(`Promesa ${obj.value} y ${obj.result}`);
    return cuadradoPromise('8');
})
.then((obj)=>{
    console.log('Inicio de la quinta promesa');
    console.log(`Promesa ${obj.value} y ${obj.result}`);
    console.log('Fin de la promesa')
})
.catch(err =>{
    console.error(err)
});