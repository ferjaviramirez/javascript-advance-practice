//el uso de los proxies: Es un nuevo mecanismo que nos permite crear un objeto que viene de un literal, en este caso el uso hace una copia entre el objeto original y el nuevo,
// dicho esto toda propiedad que se agregue va estar tanto en original como en copia.


const persona = {
    nombre: "",
    apellido: "",
    edad: 0
}


const manejador = {
    set(obj, prop, valor){
        if(Object.keys(obj).indexOf(prop) === -1){
            return console.error(`La propiedad ${prop} no existe en persona!`)//Esta es una validacion para que solo acepte las propiedades permitidas.
        }

        if(
            (prop === "nombre" || prop === "apellido") && !(/^[a-zá-üñ\s]+$/ig.test(valor))
        ){
            return console.log('la propiedad solo acepta letras y espacios en blanco');
        }
        obj[prop] = valor;
    }
}


const fernando = new Proxy(persona, manejador);
fernando.nombre = 'fernando';
fernando.apellido = 'ramirez';
fernando.edad = 30;
fernando.facebook = "ferajavi40";


console.log(fernando);
console.log('Este es el objeto', persona);
