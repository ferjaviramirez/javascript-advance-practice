// (()=>{

//     console.log('codigo Sincrono');
//     console.log('Inicio');

//     function dos(){
//         console.log('Dos');  
//     }
//     function uno(){
//         console.log('Uno');
//         dos();
//         console.log('Tres');
//     }


//     uno();
//     console.log('Fin');

// })();

(()=>{

    console.log('codigo ASincrono');
    console.log('Inicio');


    function dos(){

        setTimeout(()=>{
            console.log('dos')
        },2000)
        
    }

    function uno (){
        setTimeout(()=>{
            console.log('uno');
        },0)
        dos();
        console.log('tres');
    }

    uno();

})();


