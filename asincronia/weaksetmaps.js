//WEAK-MAPS Estos elementos no pueden ser iterables, asi que no podemos usar FOR, no tiene la propiedad size, funciona GET , SET , HAS .

//asi no se pueden almacenar los valores, tiene que usarse el metodo SET y de uno en uno:
//const weakSet = new WeakSet([1,2,3,3,4,5,5,5,6,7,true, true, "hola"]);

const weakSet = new WeakSet();

let valor1 = {"valor1":1};
let valor2 = {"valor2":2};
let valor3 = {"valor3":3};


weakSet.add(valor1);
weakSet.add(valor2);
weakSet.add(valor3);


console.log(weakSet);


console.log('******************************** Probando metodos *****************************************');

weakSet.delete(valor2);
console.log(weakSet);


// setInterval(()=> console.log(weakSet),1000);


//esto es para limpiar las referencias:
// setTimeout(()=>{
//     valor1 = null;
//     valor2= null;
//     valor3 = null;
// },5000);


console.clear();

// aqui esta el WeakMap, no podemos iterarlos ni tampoco ver el size, 
const weakMap = new WeakMap();

let llave1 = {};
let llave2 = {};
let llave3 = {};


weakMap.set(llave1,1);
weakMap.set(llave2,2);
weakMap.set(llave3,3);

console.log(weakMap);

console.log('Esto me trae el valor: ',weakMap.get(llave1));

setInterval(()=> console.log(weakMap),1000);

setTimeout(()=>{
    llave1 = null;
    llave2 = null;
    llave3 = null;
},5000);
