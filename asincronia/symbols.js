// como podemos el Symbol es un tipo de dato primitivo que asigna un ID especifico a cada propiedad, es decir para que no hay problemas que se sobrescriban las propiedades

let id = Symbol('Hola');
let id2 = Symbol('Hola');

console.log(id=== id2);

console.log(typeof id , typeof id2);

// y asi se usa en un objeto: dentro del parentesis podemos poner referencias

const NOMBRE = Symbol('Nombre');
const SALUDAR = Symbol('Saludar');

//otra ventaja es que hace que la llave del objeto permanezca privada, esto nos sirve para ocultar datos en consumo de una API
const persona = {
    [NOMBRE]: 'Fernando',
    edad: 31
}

//tambien como puedes ver aqui no nos permite sobre escribir los datos.
persona.NOMBRE = 'Javier';

console.log(persona);

//para mandar a llamar una propiedad dentro de objeto con un simbolo se usa:
console.log('Este es la propiedad que hemos creado: ', persona.NOMBRE)
console.log('Este es el simbolo que estamos llamando ',persona[NOMBRE]);

persona[SALUDAR]= function() {
    console.log('Hola mundo desde Java');
}

console.log(persona);

//asi se llama a una funcion que usa symbol
persona[SALUDAR]();


//Aqui podemos ver que no nos va pintar todas las propiedades ya que estan privadas, solo pinta las publicas las que no se han hecho con SYMBOL, para mostrarlas existe otra forma
for(let propiedad in persona){
    console.log(propiedad);
    console.log(persona[propiedad]);
}


console.log('*********************************************************************')

console.log(Object.getOwnPropertySymbols(persona));