
let aleatorio = Math.round(Math.random() * 100 + 5);
const objUsuarios = {
    propiedad: "valor",
    [`id_${aleatorio}`] : 'Valor aleatorio'//podemos crear propiedades dinamicas, por ejemplo dentro de este objeto pero siempre usando los corchetes.
}

const usuarios = ['Fernando', 'Javier', 'Ulrika'];


usuarios.forEach((usuario, index)=> objUsuarios[`id_${index}`]= usuario);

console.log(objUsuarios);

