//la promesas trabajan de la mano con las funciones asycronas.s


function cuadradoPromise(value) {
    if (typeof value !== 'number') return Promise.reject(`El valor ingresado ${value} no es un numero`);
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve({
                value: value,
                result: value * value
            })

        }, 0 | Math.random() * 1000);
    });
}


//Aqui podemos ver cuando se trata de una funcion declarada y una expresada, ddonde es que tiene que ir el async

async function funcionAsyncronaDeclarada() {
    try {
        console.log('Inicio de funcion asyncrona');
        let objeto = await cuadradoPromise(0);
        console.info(`Async function ${objeto.value}, ${objeto.result}`);

        objeto = await cuadradoPromise(1);
        console.info(`Async function ${objeto.value}, ${objeto.result}`);

        objeto = await cuadradoPromise(2);
        console.info(`Async function ${objeto.value}, ${objeto.result}`);

        objeto = await cuadradoPromise(3);
        console.info(`Async function ${objeto.value}, ${objeto.result}`);

    } catch (err) {
        console.log('Solo acepta valores de numero: ', err)
    }
}

const funcionAsyncronaExpresada =  async() => {
    try {
        console.log('Inicio de funcion asyncrona');
        let objeto = await cuadradoPromise(4);
        console.info(`Async function ${objeto.value}, ${objeto.result}`);

        objeto = await cuadradoPromise(5);
        console.info(`Async function ${objeto.value}, ${objeto.result}`);

        objeto = await cuadradoPromise('6');
        console.info(`Async function ${objeto.value}, ${objeto.result}`);

        objeto = await cuadradoPromise(7);
        console.info(`Async function ${objeto.value}, ${objeto.result}`);

    } catch (err) {
        console.log('Solo acepta valores de numero: ', err)
    }
}


funcionAsyncronaDeclarada();
funcionAsyncronaExpresada();