


// //Selectores.
// //aqui vamos a capturar elementos por etiqueta:
// console.log('By tag name ',document.getElementsByTagName('li'));
// //aqui vamos a capturar elementos por clase:
// console.log('By class name ',document.getElementsByClassName('card'));
// //aqui capturamos por nombre: ejmplo en los formularios:
// console.log(window.document.getElementsByName('nombre'));
// //aqui seleccionamos por id en singular:
// console.log(document.getElementById('menu'));

// //Estos son los mas famosos en selecctor: esta nos devuelve un NODE LIST no un arreglo como los anteriores

// console.log(document.querySelector('#menu'));

// console.log('este es el query selector all ',document.querySelectorAll('a'));


// document.querySelectorAll('a').forEach((element)=> console.log('Aqui esoy usando el forEach: ',element));
// console.log('Aqui esta imprimiendo el codigo de menu li ',document.querySelectorAll('#menu li'));
// console.clear();


// //atributos de etiquetas html


// //Esto es para seleccionar la etiqueta HTML
// console.log(document.documentElement.lang);
// console.log(document.documentElement.getAttribute('lang'));

// //ESTO ES MUY IMPORTANTE SABER DISTINGUIR QUE ES LA DIFERENCIA Y QUE NOS TRAE
// console.log(document.querySelector('.link-dom').href);
// //aqui podemos ver que estamos usando el getAttribute para capturar la propiedad. tambien existe el setAttribute para asignar valor
// console.log('Este lo trae de la forma correcta: ',document.querySelector('.link-dom').getAttribute('href'))

// document.documentElement.lang = "en";
// console.log(document.documentElement.lang);

// //aqui estamos settiando la propiedad: el primero es el valor que debemos capturar y el segundo el valor que vamos a poner
// document.documentElement.setAttribute('lang','deu');


// console.clear();

// //cuando declaramos una variable que tiene que ver con un elemento del dom , usamos el signo del dollar para hcer referencia  
// let $linkDom = document.querySelector('.link-dom');

// $linkDom.setAttribute('target','_blank');
// $linkDom.setAttribute('rel','noopener');
// $linkDom.setAttribute('href', 'https://www.google.com/');
// console.log($linkDom.hasAttribute("rel"));
// $linkDom.removeAttribute('rel');
// console.log($linkDom.hasAttribute("rel"));



// //DATA ATRIBUTES: Data-Atrributes:

// console.log($linkDom.getAttribute('data-description'));
// console.log($linkDom.dataset);
// console.log($linkDom.dataset.id);
// console.log($linkDom.dataset.description);
// //aqui vamos a settear 
// $linkDom.setAttribute('data-description','Modelo de objeto del documento');
// console.log($linkDom.dataset.description);
// $linkDom.dataset.description = 'Hola mundo desde España';
// console.clear();


//ESTILOS Y VARIABLES CSS.

let $anchorTags = document.querySelectorAll('a');

$anchorTags.forEach((elemento)=>{
    elemento.style.setProperty("text-decoration","none");
});

let $linkDom = document.querySelector('.link-dom');
//diferentes formas de ver los estilos de los elementos.
console.log('linkDom',$linkDom.style.backgroundColor);
console.log('get atributte ',$linkDom.getAttribute("style"));
console.log('desde window ',window.getComputedStyle($linkDom));
console.log('desde window llamando color ',window.getComputedStyle($linkDom).getPropertyValue("color"));
console.clear();

//Aplicar estilos:
$linkDom.style.setProperty("text-decoration","none");
$linkDom.style.setProperty("display","block");
$linkDom.style.width = "50%";
$linkDom.style.textAlign = "center";
$linkDom.style.marginLeft = "auto";
$linkDom.style.marginRight = "auto";
$linkDom.style.padding = "1rem";
$linkDom.style.borderRadius = ".5rem";

//Variables CSS - Custom Properties
// he declarado las variables en el style css dentro del root css
const $html = document.documentElement;
const $body = document.body;

let varDarkColor = getComputedStyle($html).getPropertyValue("--dark-color");
let varYellowColor = getComputedStyle($html).getPropertyValue("--yellow-color");

console.log('es var',varYellowColor, varDarkColor);

$body.style.backgroundColor = varDarkColor;
$body.style.color = varYellowColor;

//cambiar los valores de propiedades:
$html.style.setProperty("--dark-color", "#000");
varDarkColor = getComputedStyle($html).getPropertyValue("--dark-color");

// $body.style.setProperty("background-color", varDarkColor);

// console.clear();



//acceder a las clases usando el dom

const $card = document.querySelector(".card");

console.log($card);

//Esto es para saber si un elemento contiene una clase especifica:

console.log($card.classList.contains("rotate-45"));
$card.classList.add("rotate-45");
console.log($card.classList.contains("rotate-45"));
$card.classList.remove("rotate-45");
// $card.classList.toggle("rotate-45");
// $card.classList.toggle("rotate-45");
//Esta recibe dos arguementos: la clase que eliminas y la que agregas.
// $card.classList.replace("rotate-45","rotate-135");

// $card.classList.add("opacity-80","sepia");
// $card.classList.remove("opacity-80","sepia");
// $card.classList.toggle("opacity-80","sepia");

//Texto y HTMl DOM

const $whatIsDOM = document.getElementById("que-es");
let textoDom = `
<p>El modelo de Objetos del Documento(<b><i>DOM -  Document Object Model</i></b>) es un API para documentos HTML y XML</p>
<p>Este provee una reprentacion estructural del documento, permitiendo modificar su contenido y presentacion visual mediande codigo JS</p>
<p><mark>EL DOm no es una parte de la espcificacion de JAvascript, es una API para los navegadores</mark></p>
`;
console.log('what is dom', $whatIsDOM);



//Insertar codigo html: estos son los 3 metodos:
$whatIsDOM.textContent = textoDom;
$whatIsDOM.innerHTML = textoDom;
$whatIsDOM.outerHTML = textoDom;


//traversing : Recorriendo el DOM.

// let $cards = document.querySelector(".cards");

// console.log('cards', $cards.children);
// console.log('cards', $cards.children[2]);
// console.log('cards', $cards.parentElement);
// console.log('first element child: ', $cards.firstElementChild);
// console.log('last element child: ', $cards.lastElementChild);
// console.log('previus sibling: ', $cards.previousElementSibling);
// console.log('next sibling: ', $cards.nextElementSibling);




//Agregar elemento al dom de manera dinamica:


const $figure = document.createElement("figure");
const $img = document.createElement("img");
const $figcaption = document.createElement("figcaption");
const $figcaptionText = document.createTextNode("Animals");
const $cards = document.querySelector('.cards');


$img.setAttribute("src","https://placeimg.com/200/200/animals");
$img.setAttribute("alt","Animals");
$figure.classList.add("card");
$figcaption.appendChild($figcaptionText);
$figure.appendChild($img);
$figure.appendChild($figcaption);
$cards.appendChild($figure);

//segunda forma de generar contenido dinamico:

const $figure2 = document.createElement("figure");
$figure2.innerHTML = `
    <img src="https://placeimg.com/200/200/people" alt="People">
    <figcaption>People</figcaption>
`;
$figure2.classList.add("card");
$cards.appendChild($figure2);



// agregar elementos de forma dinamica con varios datos:

const estaciones = ["Primavera","Verano","Otoño", "Invierno"];
$ul = document.createElement("ul");

document.write("<h3>Estaciones del año</h3>");
document.body.appendChild($ul);

estaciones.forEach(el =>{
    const $li = document.createElement('li');
    $li.textContent = el;
    $ul.appendChild($li);
});


const continentes = ["Africa","America", "Asia", "Europa", "Oceania"];
const $ul2 = document.createElement("ul");

document.write("<h3>Contienentes del mundo</h3>");
document.body.appendChild($ul2);
$ul2.innerHTML ="";
continentes.forEach(el => $ul2.innerHTML += `<li>${el}</li>`);

//Fragmentos del DOM 

const meses = ["enero", "febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

 let $ul3 = document.createElement("ul");
 let $fragment = document.createDocumentFragment();

 meses.forEach(el => {
     const $li = document.createElement("li");
     $li.textContent = el;
     $fragment.appendChild($li);
 });

 document.write("<h3>Meses del año</h3>");
 $ul3.appendChild($fragment);
 document.body.appendChild($ul3);



//tambien si agregamos el metodo toggle(); este sirve como un interruptor se la quita la clase o se la agrega, hace siemre lo contrario, sirve para hacer el dark mode 


//Templates HTML.

// const $cards2 = document.querySelector(".cards");
// const $template = document.querySelector("#template-card").content;
// const $fragmento = document.createDocumentFragment();
// const cardContent = [
//     {
//         title: "Tecnologia",
//         img: "https://placeimg.com/200/200/tech"
//     },
//     {
//         title: "Animales",
//         img: "https://placeimg.com/200/200/animals"
//     },
//     {
//         title: "Arquitectura",
//         img: "https://placeimg.com/200/200/arch"
//     },
//     {
//         title: "Naturaleza",
//         img: "https://placeimg.com/200/200/nature"
//     },
//     {
//         title: "Gente",
//         img: "https://placeimg.com/200/200/people"
//     },
//     {
//         title: "Tecnologia",
//         img: "https://placeimg.com/200/200/tech"
//     }
// ]


// cardContent.forEach(el => {
//     $template.querySelector("img").setAttribute("src", el.img);
//     $template.querySelector("img").setAttribute("alt", el.title);
//     $template.querySelector("figcaption").textContent = el.title;


//     let $clone = document.importNode($template,true);
//     $fragmento.appendChild($clone);
// });

// $cards2.appendChild($fragmento);


// const $cards3 = document.querySelector(".cards");
// const $newCard = document.createElement("figure");
// const $cloneCards = $cards3.cloneNode(true);//Esto es para clonar la seccion de cartas.


// $newCard.innerHTML = `
//     <img src="https://placeimg.com/200/200/any" alt="Any">
//     <figcaption>Any</figcaption>
// `;

// $newCard.classList.add("card");

//Este metodo remplaza el elemento en la posicion que nosotros deseamos.
// $cards3.replaceChild($newCard,$cards3.children[2]);

//Este metodo hace que se inserte antes de la primera: Este no lo remplaza simplemente lo inserta antes. necesita 2 parametros
// $cards3.insertBefore($newCard, $cards3.firstElementChild);

//Este metodo es para eliminar un elemento hijo o nodo.
//$cards3.removeChild($cards3.lastElementChild);

//Este metodo sirve para clonar la seccion de elementos dinamicamente:
// document.body.appendChild($cloneCards);


//Otras formas de modificar elementos del DOM.

//insertAdjacent...
//insertAdjacentElement(position,el);
//insertAjacentHTML(position,html);
//insertAdjacentText(position, text);

//Posiciones:
//beforebegin(hermano anterior);
//afterbegin(primer hijo);
//beforeend(ultimo end);
//afterend(hermano siguiente);


const $cards3 = document.querySelector(".cards");
const $newCard = document.createElement("figure");

let $contentCard = `
    <img src="https://placeimg.com/200/200/any" alt="Any">
    <figcaption></figcaption>
`;

$newCard.classList.add("card");
$newCard.insertAdjacentHTML("beforeend",$contentCard);
$newCard.querySelector("figcaption").insertAdjacentText("afterbegin","Any");
// $cards3.insertAdjacentElement("afterbegin",$newCard);

//Estos son metodos de jquery que ahora puedes usar en javascript:
//Este lo agrega al inicio del padre, como primer hijo
// $cards3.prepend($newCard);
//Este lo agrega como ultimo hijo
// $cards3.append($newCard);
//Este lo agrega como hermano anterior: 
// $cards3.before($newCard);
//Este lo agrega como hermano posterior: 
//$cards3.after($newCard);


