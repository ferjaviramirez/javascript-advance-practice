//esta es la destructuracion 

let numeros = [1, 2, 3, 4];
let [uno,dos,tres,cuatro] = numeros
console.log(uno);


//en los objetos la variable tiene que tener el mismo valor que la variable , 
//es diferente caso que en los arrays no importa sino se respeta el orden.
let persona = {
    nombre: "Fernando",
    numero: 254658451,
    edad: 30
}
let {nombre, numero, edad} = persona;
console.log(nombre, numero, edad);