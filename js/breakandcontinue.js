//break and continue, estas estan destinadas para ser usadas en estructuras de control como
//for, while... no podemos usarla en el metodo forEach de Arrays


const numeros = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];


//el break rompe la iteracion
for (let i = 0; i < numeros.length; i++) {
    if (i === 5) {
        break;
    }
    console.log('Esto es usando break ',numeros[i]);
}
// conitue solo se salta el elemento de la iteracion y continua
//aqui el continue se salta la posicion 5 del index en este caso seria el 6
for (let i = 0; i < numeros.length; i++) {

    if (i === 5) {
        continue;
    }
    console.log('Esto es el continue ',numeros[i]);
}


numeros.forEach((elementos) => {
    console.log('Estos son usando for each ',elementos);
});