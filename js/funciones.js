//Funciones de javascript
//esta es una funcion declarada, se usa la palabra reservada function
function saludar() {

    setTimeout(() => {
        console.log('Hola mundo');
    },Math.random()*1000);

    setTimeout(() => {
        console.log('Hola Luna');
    }, 2000);

    setTimeout(() => {
        console.log('Hola Marte');
    }, 3000);

}
// saludar();

//Funcion declarada que devuelve un valor

function devuelve() {
    console.log("uno");
    console.log("dos");
    console.log("tres");
    return "Esto es lo que nos retorna la funcion";
}
// let resultado = devuelve();
// console.log(resultado);

//Funcion declarada que recibe paramatros

function despedir(nombre, apellido) {
    return `hola ${nombre} tu apellido es ${apellido}`;
}

let adios = despedir('Fernando', 'Ramirez');
// console.log(adios);


//funciones declaradas versus funciones expresadas
function funcionDeclarada() {
    console.log('Esto es una funcion declarada, puede invocarse en cualquier parte de nuestro codigo')
}

//funcion anonima
const funcionExpresada = function () {
    console.log('Esto es una funcion expresada, se le ha asignado como valor a una variable, si la invocamos despues de haber sido declarada');
}




//funcion declarada:
function sumar(a=0,b=0){
    return (a+b)
}
resultadoSuma= sumar(10,10);
console.log(resultadoSuma);

//funcion expresada:Esta es una funcion anonima que se le ha asiganado como valor a una variable
let resta = function(a=0,b=0){
    return a - b
}
console.log(resta(10,5));

//funcion de flecha:
let multiplicacion = (a=0, b=0) =>{
    if(a===0 || b===0){
        return console.log('No se admiten valores nulos');
    }
    return a * b;
}
// multiplicacion(0,10);




let despedirse = (saludo="Desconocido", dia=0) =>{
    if(typeof(saludo)!=='string' || saludo.length <= 5){
        return console.log(`Este valor ${saludo} no es valido`);
    }
    if(typeof(dia)!=='number'){
        return console.log('Solo se aceptan numeros');
    }
    console.log(`${saludo} y este es el dia ${dia}`);

}

despedirse('buenos dias', 30);
despedirse();
