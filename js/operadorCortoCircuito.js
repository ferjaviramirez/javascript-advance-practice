//Operador de corto circuito

//Corto circuito OR cuando el valor de la izquierda en la epxresion siempre
//pueda validar a true, es el valor que se cargara por defecto
function saludar(nombre) {
    nombre = nombre || "Desconocido";
    console.log(`Hola ${nombre}`);
}


saludar("fernando");

//Este valor tiende a true
console.log(23 || "Valor de la derecha");

//estos valores tienden a falsos
console.log(false || "Valor de la derecha");
console.log(undefined || "Valor de la derecha");
console.log(null || "Valor de la derecha");

//Corto circuito AND - cuando el valor de la izquierda siempre puede validar a false
// es valor que se encargara por defecto.
console.log(23 && "Valor de la derecha");


console.log(false && "Valor de la derecha");
console.log(undefined && "Valor de la derecha");
console.log(null && "Valor de la derecha");

console.clear();


saludar();
console.log(23 && "Valor de la derecha");
console.log(false && "Valor de la derecha");

