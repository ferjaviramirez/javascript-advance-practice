var hola="Hola mundo";
let hello = "hello world";
console.log(hola);
console.log(window.hola);
console.log(window.hello);


//Aqui estamos probando el hoisting , el alcanze de las variables.

// var musica = "rock";
// console.log('Esto es antes del bloque', musica);
// {

//     var musica ="pop";
//     console.log('Esto es dentro del bloque', musica);

// }

// console.log('Esto es dentro del bloque', musica);

let objeto = {
    nombre: 'Fernando',
    edad: 23
};

let colores = ['Azul', 'verde', 'rojo'];


console.log(objeto);
objeto.edad= 40;


objeto.ano = "1990";
console.log(objeto);

colores.push('amarillo');
console.log(colores);
