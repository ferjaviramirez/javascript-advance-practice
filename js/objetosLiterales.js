//objetos literales de ecmascript

let nombre = "fErNaNdo";
let edad = 23;

const persona = {
    nombre: nombre,
    edad: edad,
    ladrar: function () {
        console.log('Wow wow')
    }
};

console.log(persona.nombre);
persona.ladrar();

//aqui estamos haciendo un nuevo arreglo y le vamos asignar a sus propiedades el valor de las
//variables si declararlas, es decir javascript las agrega dinamicamente
//y tambien una nueva forma de declarar metodos(funciones) dentro de objetos
const persona2 = {
    nombre,
    edad,
    numero: 25458455,
    ladrar() {
        console.log('Vamos a ladrar');
    }
}

console.log(persona2);