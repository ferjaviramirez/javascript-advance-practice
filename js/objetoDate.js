let fecha = new Date;

console.log(fecha);
console.log(fecha.getDate());
//dia de la semana, los dias empienzan los domingos en JS
console.log(fecha.getDay());
//aqui lo estamos pasando a string
console.log(fecha.toString());
console.log(fecha.toDateString());
console.log(fecha.toLocaleDateString());
console.log(fecha.toLocaleTimeString());
console.log(Date.now());

let cumpleFer = new Date(1990,1,4,10,0,30);
console.log(cumpleFer);
console.clear();

let fechaActual = new Date();

console.log(fechaActual.getFullYear());
console.log(fechaActual.getHours());
console.log(fechaActual.getMinutes());
console.log(fechaActual.getSeconds());
console.log(fechaActual.toLocaleString());
console.log(fechaActual.toLocaleTimeString());
console.log(fechaActual.getTimezoneOffset());
console.log(Date.now());
console.clear();

let cumpleanosFer = new Date(1990,2,4);
console.log(cumpleanosFer);