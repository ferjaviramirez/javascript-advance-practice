//manejo de errores


// try {
//     console.log('En el try se agrega el codigo a evaluar');
//     noExiste;
//     console.log('segundo mensaje en el try');
// } catch (error) {
//     console.log('Catch captura cualquier error surgido o lanzado en el try');
//     console.log(error);
// } finally {
//     console.log('Finally se ejecutara siempre al final de un bloque try-catch')
// }


try {
    let numero = "x";
    if (isNaN(numero)) {
        throw new Error(`${numero} no es un numero`);
    }
    console.log(numero * numero);

} catch (error) {
    console.log(`Se produjo el siguiente error: ${error}`)
} finally {
    console.log('Este siempre se va a ejecutar, casi nunca se utiliza');
}


try {
    console.log('codigo a evaluar');
    noExiste;
    console.log('Paso el error');

} catch (error) {
    console.log(`imprime el error ${error}`);

} finally {
    console.log('siempre se va a ejecutar');
}