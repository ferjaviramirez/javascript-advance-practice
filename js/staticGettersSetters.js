class Animal {

    constructor(nombre, genero) {
        //El contructor es un metodo especial que se ejectuta
        //en el momento de instanciar la clase
        //atributos
        this.nombre = nombre;
        this.genero = genero;

    }
    //metodos
    sonar() {
        console.log('Hace sonidos porque estoy vivo');
    }

    saludar() {
        console.log(`Hola ${this.nombre}  este es tu genero ${this.genero}`)
    }

    static despedirse(){
        console.log('Hola esto se ejecuta usando un static');
    }

}

// Animal.despedirse();

//Este es una herencia prototipica de la clase animal para la clase de perro
class Perro extends Animal {
    constructor(nombre, genero, tamanio) {
        //El super es el metodo que manda a llamar al contructor del padre
        super(nombre, genero);
        this.tamanio = tamanio;
        this.raza = null;
    }

    sonar() {
        console.log("Soy un animal y hago sonidos!!");
    }

    ladrar() {
        console.log("Guaooo Guaoo");
    }

    //Un metodo statico es aquel que se puede ejecutar sin necesidad de instanciar la clase
    static queEres() {
        console.log('Los perros son animales mamiferos amigos del hombre');
    }

    //los setters y getters son metodos especiales que nos 
    //permiten  establecer y obtener los valores de atributos
    //de nuestra clase
    get getRaza() {
        return this.raza;
    }

    set setRaza(raza) {
        this.raza = raza;
    }
}


// Perro.queEres();

const mimi = new Animal('Mimi', 'Femenino');
const scooby = new Animal('Scooby', 'Masculino');
const ulrika = new Perro('Ulrika', 'Femenino', 20);

//aqui ponemos en practica los getters y setters
// ulrika.sonar();
// console.log(ulrika.getRaza);
// ulrika.setRaza = "Es necia";
// console.log(ulrika.getRaza);

// console.log(mimi);
// console.log(scooby);
// console.log('Esto es ulrika', ulrika);

// mimi.saludar();
// scooby.saludar();



class Square{
    constructor(ancho){
        this.ancho = ancho;
        this.largo = ancho;
    }
    get area(){
        return this.ancho * this.largo;
    }
}


class Triangle extends Square{
    constructor(altura,ancho){
        super(ancho);
        this.altura = altura;
        this.incrementoContador = 0;
    }

    get areaTriangulo (){
        this.incrementoContador++;
        return (this.ancho*this.altura /2);
    }

    set valoresTriagulo(area){
        this.altura = area / 2;
        this.ancho = area / 2;
    }
}


let cuadro1 = new Square(25);
let triangulo = new Triangle(10,10);
console.log('Este es el valor antes del setter: ', triangulo.altura);
triangulo.valoresTriagulo = 2;
console.log('Este es el valor despues del setter: ', triangulo.altura);
console.log(triangulo.areaTriangulo);
console.log(triangulo.areaTriangulo);
console.log(triangulo.areaTriangulo);
console.log(triangulo.areaTriangulo);
console.log(triangulo.incrementoContador);

