//los objetos de javascript


//los objetos dentro de un objeto a las variables se les va llaamr atributos/propiedades y las funciones se les llama metodos
// const fernando = {
//     nombre: 'fernando',
//     edad: 31,
//     apellido: 'ramirez',
//     pasatiempos: ['correr', 'comer', 'jugar'],
//     contacto: {
//         numeroTel: 45645645646,
//         direccion: 'calle bajondillos'
//     },
//     saludar: function () {
//         console.log('Hola mundo')
//     },
//     decirMiNombre: function () {
//         console.log(`Hola me llamo ${this.nombre} y tengo esta edad ${this.edad}`)
//     }
// }


// fernando.saludar();
// fernando.decirMiNombre();
// console.log(fernando['edad']);
// console.log(fernando.pasatiempos[1]);

// //Propiedades

// console.log(Object.keys(fernando));
// console.log(Object.values(fernando));

let a = new String('Hola');
// console.log(a);

const b = {
    nombre: 'Fernando',
    edad: 31,
    saludo: 'Hola mundo',
    contacto:{
        email:'ferjaviramirez@gmail.com',
        movil: 356847854552
    },
    hobbies: ['cantar', 'comer', 'soñar'],
    saludar: (nombre = this.nombre, edad = this.edad, saludo = this.saludo) => {
        return console.log(`hola ${nombre}, esta es tu edad ${edad} y este es tu saludo ${saludo}`)
    }
}

b.saludar('javier', 23, 'hola españa');



let agregarArreglo = (arreglo) => {
    if (!arreglo) return console.log('por favor ingrese un array');
    if (!Array.isArray(arreglo)) return console.log(`Esto ${arreglo} no es un Array, ingrese de nuevo por favor`);
    setTimeout(() => {
        arreglo.map((elemento) => {
            elemento.toUpperCase();
            return console.log(elemento.charAt(0).toUpperCase() + elemento.substr(1).toLowerCase())
        })
    },Math.random() * 1000);
}

agregarArreglo(b.hobbies);
agregarArreglo();
agregarArreglo(30);

console.log(b.contacto.email);
console.log(b.hobbies.length -1);
//metodos para los objetos:
//Esto es para sacar las llaves de un bojeto:
console.log('estas son las llaves: ',Object.keys(b));
//esta es para sacarlos valores de las llaves:
console.log('estas son las valores: ',Object.values(b));
//esta para saber si existe la propiedad dentro del objeto y nos devolvera un boolean: 
console.log(b.hasOwnProperty('edad'));


Object.getOwnPropertyNames(b).forEach((val, idx, array)=>{
    console.log(val + '=>' + b[val]);
});