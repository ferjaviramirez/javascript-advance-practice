//los parametros rest, Este parametro c puede recibir cantidad infinita de valores y los va sumar 

function sumar(a, b, ...c) {
    let resultado = a + b;

    c.forEach((n) => {
        resultado += n;
    })
    return resultado;
}

console.log(sumar(1, 2));
console.log(sumar(1, 2, 3));
console.log(sumar(1, 2, 3, 4));


//Spread operator, operador de propagacion
//en este caso sirve para concatenar los dos arreglos anteponiendo los ... antes del nombre del array
//esto no modifica los arreglos originales, crea uno nuevo a partir de los anteriores

const arr1 = [1,2,3,4,5,6];
const arr2 = [7,8,9,10,11,12];

const arr3 = [...arr1 , ...arr2];

console.log(arr3);

