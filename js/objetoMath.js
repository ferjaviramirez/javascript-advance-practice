//objeto math, este es un objeto estatico. tenemos que invocar al objeto

console.log(Math);
console.log(Math.PI);
console.log(Math.abs(-5));
//el ceil busca siempre el mayor posterior y el floor el menor
console.log(Math.ceil(8.6));
console.log(Math.floor(8.6));

console.log(Math.round(5.5));

console.log(Math.pow(8,5));

//este metodo sign es para saber cuando un numero es positivo, negativo o cero
console.log(Math.sign(-8));

console.log(Math.round(Math.random()*100));
console.clear();



console.log(Math.round(Math.random()*1000)+1);