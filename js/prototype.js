//Prototype POO

// Clases: Es un modelo a seguir
//Objetos: Esta una copia de una clase tienen Atributos y metodos
//atributos: son variables dentro de un Objeto
//metodos: son acciones que un objeto puede hacer por ejm GET : getelementById y son funciones dentro de objeto

//en resumen esto seria el como un objeto puede heredar de un objeto padre los atributos y metodos


// const animal = {
//     nombre: "Ulrika",
//     sonar() {
//         console.log('Hace sonidos porque estoy vivo');
//     }
// }

// console.log(animal);

// //Funcion constructora

// function Animal(nombre, genero) {
//     //atributos
//     this.nombre = nombre;
//     this.genero = genero;

// }
// //metodos
// Animal.prototype.sonar = function () {
//     console.log('Hace sonidos porque estoy vivo');
// }

// Animal.prototype.saludar = function () {
//     console.log(`Hola ${this.nombre}  este es tu genero ${this.genero}`)
// }

// //herencia prototipica
// function Perro(nombre, genero, tamanio) {
//     this.super = Animal;
//     this.super(nombre, genero);
//     this.tamanio = tamanio;
// }


// //Perro esta heredando de Animal
// Perro.prototype = new Animal();
// Perro.prototype.constructor = Perro;

// //Sobreescribir un metodo del prototipo padre al hijo
// Perro.prototype.sonar = function () {
//     console.log(`Soy un perro y mi sonido es ladrido`);
// }

// Perro.prototype.ladrar = function () {
//     console.log('Guauu Guauuu');
// }


// const snoppy = new Animal("Snoopy", "Male", "Medium");
// const LolaBunny = new Animal("Lola Bunny", "Female");

// const snoppy2 = new Perro("Ulrika", "Female", "Tall");


// console.log(snoppy);
// console.log(snoppy2);
// snoppy2.sonar();
// snoppy2.ladrar();
// snoppy.sonar();
// LolaBunny.saludar();



//Funcion constructora:

//Esta es la version 1 del prototipo: como podemos ver en esta tenemos  los metodos dentro de la funcion constructora
// function Animal(nombre, genero) {
//     this.nombre = nombre;
//     this.genero = genero;

//     this.sonar = function () {
//         console.log(`este es mi nombre ${this.nombre} y este es mi genero ${this.genero}`);
//     }

//     this.saludar = function(){
//         console.log(`Hola como estas ${this.nombre}`);
//     }

// }

//Esta es la version 2 de la funcion constructora: los metodos se asignan al prototipo no a la funcion
//constructora

function Animal(nombre, genero) {
    this.nombre = nombre;
    this.genero = genero;
}
//medotos agregados al prototipado de la funcion constructora:
Animal.prototype.sonar = function () {
    console.log(`este es mi nombre ${this.nombre} y este es mi genero ${this.genero}`);
}

Animal.prototype.saludar = function(){
    console.log(`Hola como estas ${this.nombre}`);
}

//estas son dos instancias del prototipo animal
const ulrika = new Animal('Baby robin', 'Macho');
const fernando = new Animal('Fernando', 'Javier');


ulrika.sonar();
ulrika.saludar();
fernando.sonar();
fernando.saludar();