//Object console
// console.log("Esto es un info");
// console.error("Esto es un error");
// console.warn("Esto es una advertencia");
// console.info("Esto es una informacion");


// let nombre = "Fernando";
// let apellido = "Ramirez";
// let edad = 31;

// console.log(nombre, apellido, edad);

// console.log(document);
// console.clear();

// console.group("Los cursos de fernando ramirez");
// console.log("Curso de angular");
// console.log("Curso de react");
// console.groupEnd();

// console.log("Estos son mis grupos");

// const numeros = [1, 2, 3, 4];
// const vocales = ["a", "b", "c", "d"];

// console.table(numeros);
// console.table(vocales);
// console.clear();

// console.time("Cuanto tiempo tarda mi codigo");
// const arreglo = Array(100000);

// for (let i = 0; i < arreglo.length; i++) {
//     arreglo[i] = i;
// }

// console.timeEnd("Cuanto tiempo tarda mi codigo");
// console.clear();

console.time('Cuanto tiempo tarda mi codigo');
const arreglo = new Array(1000);

for(let i = 0; i < arreglo.length; i++){
    arreglo[i] = i;
}

console.timeEnd('Cuanto tiempo tarda mi codigo');

console.clear();


//Esto es para hacer pruebas unitarias utilizando el console.assert
let num1 = 4;
let num2 = 3;
let prueba = "Se espera que numero num1 sea siempre menor";

console.assert(num1<num2,{num1,num2,prueba});