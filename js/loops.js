//los Loops , bucles


//while

let contador = 0;

while (contador < 10) {
    console.log('while', contador)
    contador++;
}

do {
    console.log('do while', contador)
    contador++;
} while (contador < 10);

//este es el bucle mas utilizado es este.

for (let i = 0; i < 10; i++) {
    console.log("for", i);
}

const numero = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];

for (let i = 0; i < numero.length; i++) {
    console.log('Este es el valor de i ', i)
}


//tambien este metodo hace lo mismo que el for 
numero.forEach((elemento, index) => {
    console.log('Esto es foreach', elemento, index)
})

// tenemos el for in que es algo especial para recorrrer un objeto, este nos permite iterar todas las propiedades de un
//objeto

const persona = {
    nombre: 'fernando',
    edad: 23,
    apellido: 'Ramirez'
}

for (const propiedad in persona) {
    console.log(`esta es la propiedad ${propiedad} y este su valor ${persona[propiedad]}`);
}

//ahora vamos con el  for of que este nos permite iterar todos los elementos de cualquier array que
//sea iterable en JS tambien puede iterar cadenasd de texto

const numero2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];

for (const elemento of numero2) {
    console.log('Este es el for of ',elemento)
}




const losNumeros = [1,2,3,4,5,6,7,8,9,10];

for(let i = 0; i < losNumeros.length; i++){
    console.log(`Este es el numero dentro: ${i}`);
}

losNumeros.forEach((value,index)=>{
    console.log(`Este es el valor ${value}`);

});