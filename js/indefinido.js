//Undefined, null, NaN
//ambas variables tienen el valor ausente

//Undefined significa que la variable ha sido inicializada pero el valor todavia esta ausente, osea esto lo pone Javascript
let indefinida;
console.log(indefinida);


//esta variable hemos puesto nosotros intencionalmente de que esta vacia.
let nulo = null;
console.log(nulo);


//NaN not a number
let noEsUnNumero = "hola" * 3.7;
console.log(noEsUnNumero);