//arrays o arrreglos

const a = [];
const b = ['fer', 'javi', 'carlos', 'Hola', ['ayer', 'Mañana', 'Hoy']];

a.push('Fernando');
// console.log(a);
// console.log(b);
// console.log(b[3]);
// console.log('Este es el ultimo', b[b.length - 1])
// console.log(b[4][1]);

//Esta es otra forma de hacer arreglos
const d = Array.of("x", "Y", "Z", 1, 2, 3);
// console.log(d);

//Otra forma de hacer arreglos.
const e = Array(100).fill(false);
// console.log(e);

const f = new Array();
// console.log(f);

const colores = ['blanco', 'verde', 'azul','negro'];
colores.forEach(function (element, index) {
    console.log(`<li id="${index+1}">${element}</li>`)
});

let words = ['uno', 'dos', 'tres', 'cuatro'];

words.forEach(function (word) {
    if (word === 'tres') {
        words.push('si se ha encontrado');
        return words
    }
});

// let mundo = words;
// console.log('Este es mundo', mundo);


//Filter
let palabras = ['spray', 'limit', 'elite', 'exuberant', 'destruction', 'present'];
let resultado = palabras.filter((word) => {
    return word.length < 6;
});

// console.log(resultado);


// const ab = [1,2,3,4,5,6,[1,2,3]];
// console.log(ab);
// console.log(ab[3]);
// console.log(ab[ab.length -1][0]);

