//arrow functions, NO SE RECOMIENDA USAR ARROW FUNCTION DENTRO DE OBJETOS, por que el this se engloba de
//al windows y no al scope donde se encuentra 

const saludo = () => {
    console.log("Hola");
}

saludo();

//esta funcion de flecha la hemos hecho mas expresiva aun, quitando los parentesis y corchetes
const despedir = despedida => console.log(`esta es despedida de ${despedida}`);

despedir("Carlos");


// const sumar = (a, b) => {
//     return a + b;
// }
//en esta funcion esta funcionando haciendo un return explicito, hemos omitido poner el return
const sumar = (a, b) => a + b;
console.log(sumar(8, 9));

const funcionDeVariasLineas = () => {
    console.log('Uno');
    console.log('Dos');
    console.log('Tres');
}


const numeros = [1, 2, 3, 4, 5, 6, 7, 8, 9];
numeros.forEach((element, index) => console.log(`El elemento ${element} esta en la posicion ${index}`));

const perro = {
    nombre: 'Ulrika',
    ladrar() {
        console.log(this)
    }
}

perro.ladrar();