//las expresiones regulares es una secuencia de caracteres que hacen un patron de busqueda
//principalmente usados en las cadenas de caracteres


let cadena = "Lorem ipsum dolor sit amet, consectetur lorem adipisicing elit. Eligendi non quis exercitationem culpa nesciunt nihil aut nostrum explicabo reprehenderit optio amet ab temporibus asperiores quasi cupiditate. Voluptatum ducimus voluptates voluptas?";

let expReg = new RegExp("lorem","ig");//lo que esta despues del primer parametro del constructor son las flags(banderas)que cada una tiene su significado.

let expReg2 = /lorem/ig;//lo que esta despues del primer parametro del constructor son las flags(banderas)que cada una tiene su significado.

//test de devuelve un boolean, si lo encuentra  o no..
console.log(expReg.test(cadena));
//exec te devuelve un arreglo con los valores...
console.log(expReg.exec(cadena));

console.clear();

console.log(expReg2.test(cadena));
console.log(expReg.test(cadena));
console.log(expReg.exec(cadena));
console.clear();

let numeros = [7,8,9];

let encNumeros = /[1-6]/g;

console.log(encNumeros.test(numeros));