//clases y herencias en Javascript
//las clases no reciben parametros.

class Animal {

    constructor(nombre, genero) {
        //El contructor es un metodo especial que se ejectuta
        //en el momento de instanciar la clase
        //atributos
        this.nombre = nombre;
        this.genero = genero;

    }
    //metodos
    sonar() {
        console.log('Hace sonidos porque estoy vivo');
    }

    saludar() {
        console.log(`Hola ${this.nombre}  este es tu genero ${this.genero}`)
    }

}

//Este es una herencia prototipica de la clase animal para la clase de perro
class Perro extends Animal {
    constructor(nombre, genero, tamanio) {
        //El super es el metodo que manda a llamar al contructor del padre
        super(nombre, genero);
        this.tamanio = tamanio;
    }

    sonar() {
        console.log("Soy un animal y hago sonidos!!");
    }

    ladrar() {
        console.log("Guaooo Guaoo");
    }
}


const mimi = new Animal('Mimi', 'Femenino');
const scooby = new Animal('Scooby', 'Masculino');
const ulrika = new Perro('Ulrika', 'Femenino', 20);
// ulrika.sonar();

// console.log(mimi);
// console.log(scooby);
// console.log('Esto es ulrika', ulrika);

// mimi.saludar();
// scooby.saludar();



// function Animal2(nombre, genero) {
//     this.nombre = nombre;
//     this.genero = genero;

// }
// Animal2.prototype.sonar = function () {
//     console.log('Hace sonidos porque estoy vivo');
// }

// Animal2.prototype.saludar = function () {
//     console.log(`Hola ${this.nombre}  este es tu genero ${this.genero}`)
// }

// function Gato(nombre, genero, tamanio){
//     this.super = Animal2;
//     this.super(nombre, genero);
//     this.tamanio = tamanio;
// }

// //gato esta herendando de Animal2:
// Gato.prototype = new Animal2();
// Gato.prototype.constructor = Gato;

// Gato.prototype.sonar = function (){
//     console.log('Soy un gato y mi sonido es Miau');
// }

// Gato.prototype.maullar = function (){
//     console.log('Miau miauuuuu');
// }




// const firulais = new Gato('firulais','sin genero', 20);

// firulais.maullar();
// console.log(firulais);




class Persona{

    constructor(nombre, edad){
        this.nombre = nombre;
        this.edad = edad;
    }
    saludo(){
        console.log(`hola ${this.nombre}`);
    }
    despedida(){
        console.log(`hasta luego y esta es tu edad ${this.edad}`);
    }
}

class PersonaNueva extends Persona{
    constructor(nombre, edad, origen){
        super(nombre, edad);
        this.origen = origen;
    }
    saludo(){
        console.log(`Hola estoy sobrescribiendo saludo ${nombre}`);
    }
    despedida(){
        console.log(`hasta luego y estoy sobrescriendo tu edad ${this.edad} y tu origen ${this.origen}`);
    }
}

const personaNueva2 = new PersonaNueva('Rooney', 'Wayne', 'inglaterra');
console.log(personaNueva2);

personaNueva2.despedida();


class Animal3 {
    constructor(raza, precio){
        this.raza = raza;
        this.precio = precio;
    }
    tipoRaza(){
        return console.log(`Esta es la raza ${this.raza}`);
    }
    tipoPrecio(){
        return console.log(`Este es el precio ${this.precio}`);
    }
}


let fernando = new Persona('Fernando',31);
let ulrika2 = new Animal3('Yorkshire', 400);

console.log(fernando);
console.log(ulrika2);
ulrika2.tipoPrecio();
ulrika2.tipoRaza();
