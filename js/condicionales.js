// los condicionales y las estructuras de control

//if else 

let edad = 17;


if (edad >= 18) {
    console.log('Tienes la edad suficiente')
} else {
    console.log('No tienes la edad suficiente')
}

//operador ternario (condicion)? verdadero : falso

let permiso = (edad >= 18) ? "Tienes edad suficiente" : "No tienes edad suficiente";
console.log(permiso);


// switch case en los dias de la semana dando valor de 0 desde domingo

let dia = 1;

switch (dia) {
    case 0:
        console.log('Domingo')
        break;

    case 1:
        console.log('Lunes')
        break;
    default:
        console.log('El dia no existe')
        break;
}


