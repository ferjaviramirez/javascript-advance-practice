//funciones auto ejecutadas, es una funcion donde tu englobas todo el codigo que quieres ejecutar


(function () {

    console.log("Mi primer IIFE");

})();

(function (d, w, c) {

    console.log("Mi segunda IIFE");
    console.log(w);
    console.log(d);
    c.log("Este es modo corto de llamar a console")
})(document, window, console);


!function () {
    console.log('Version facebook');
}();

console.clear();


(function(){
    console.log('funcion autoejecutable');
})();